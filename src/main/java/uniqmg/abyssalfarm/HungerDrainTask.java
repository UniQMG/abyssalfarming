package uniqmg.abyssalfarm;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

class HungerDrainTask extends BukkitRunnable {
    private AbyssalFarm plugin;

    HungerDrainTask(AbyssalFarm plugin) {
        this.plugin = plugin;
    }

    void invoke() {
        this.runTaskTimer(plugin, 0, 1);
    }

    @Override
    public void run() {
        for (Player player: plugin.getServer().getOnlinePlayers()) {
            player.setExhaustion((float) (player.getExhaustion() + 80d / 24000));
        }
    }
}
