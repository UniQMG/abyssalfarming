package uniqmg.abyssalfarm.water;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.Levelled;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.abyssalfarm.AbyssalFarm;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Set;

public class RainBasinFiller extends BukkitRunnable {
    /**
     * The maximum amount of free space an empty block can have
     * around it before it becomes invalid for rain flooding
     */
    private int rainBasinSearchSize;
    private AbyssalFarm plugin;

    public RainBasinFiller(AbyssalFarm plugin, int rainBasinSearchSize) {
        this.plugin = plugin;
        this.rainBasinSearchSize = rainBasinSearchSize;
        this.runTaskTimer(this.plugin, 0, 20);
    }

    @Override
    public void run() {
        for (World world: plugin.getServer().getWorlds()) {
            if (!world.hasStorm()) continue;
            forWorld(world);
        }
    }

    private void forWorld(World world) {
        for (Chunk chunk: world.getLoadedChunks()) {
            int x = (int) (Math.random() * 16);
            int z = (int) (Math.random() * 16);

            Location baseLocation = chunk.getBlock(x, 62, z).getLocation();


            if (baseLocation.getBlock().getTemperature() > 0.95)
                continue; // Too hot for rain!

            Location loc = world.getHighestBlockAt(baseLocation).getLocation();

            // Traverse down through non-source water blocks
            while(true) {
                Block test = loc.getBlock().getRelative(0, -1, 0);

                if (test.getType() != Material.WATER || ((Levelled) test.getBlockData()).getLevel() == 0)
                    break;

                loc.add(0, -1, 0);
                if (loc.getY() <= 1) break;
            }

            Block block = loc.getBlock();
            if (testIsTank(block))
                block.setType(Material.WATER);
        }
    }

    public boolean testIsTank(Block block) {
        if (block.getType().isSolid()) return false;
        ArrayDeque<Location> queue = new ArrayDeque<>();
        Set<Location> explored = new HashSet<>();
        queue.add(block.getLocation());
        int floorGaps = 0;

        int steps = 0;
        while (queue.size() > 0) {
            if (steps++ > 10000) {
                plugin.getLogger().warning("testIsTank: Hit hard iteration limit");
                return false;
            }

            // If we've explored more blocks than the limit,
            // this isn't a valid flood target
            if (explored.size() > rainBasinSearchSize) return false;

            Location loc = queue.removeFirst();
            Block next = loc.getBlock();

            // If it's solid, we've hit a wall and shouldn't continue
            if (next.getType().isSolid()) continue;

            // If we've already been here, don't continue
            if (explored.contains(loc)) continue;
            explored.add(loc);

            // If there's more than one block missing from the floor, this isn't a valid flood target
            // A water source block counts as a valid block.
            Block below = loc.clone().add(0, -1, 0).getBlock();
            if (!below.getType().isSolid()) {
                if (below.getType() != Material.WATER || ((Levelled) below.getBlockData()).getLevel() != 0) {
                    floorGaps++;
                    if (floorGaps > 1) {
                        return false;
                    }
                }
            }

            // Explore neighboring blocks
            Location[] neighbors = new Location[] {
                loc.clone().add( 1, 0,  0),
                loc.clone().add(-1, 0,  0),
                loc.clone().add( 0, 0,  1),
                loc.clone().add( 0, 0, -1)
            };
            for (Location neighbor: neighbors) {
                // If a neighbor isn't loaded, a partial basin could form between the loaded
                // chunk and the unloaded chunk, so bail immediately.
                if (!neighbor.getWorld().isChunkLoaded(neighbor.getBlockX() >> 4, neighbor.getBlockZ() >> 4))
                    return false;
                queue.add(neighbor);
            }
        }

        return true;
    }

}
