package uniqmg.abyssalfarm.water;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Levelled;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.FluidLevelChangeEvent;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.abyssalfarm.AbyssalFarm;

public class NewWaterPhysicsManager implements Listener {
    private static final int BIOME_SEARCH_RANGE = 10;
    private static final int BIOME_SEARCH_STEP = 5;

    private AbyssalFarm plugin;
    public NewWaterPhysicsManager(AbyssalFarm plugin) {
        this.plugin = plugin;
    }

    /**
     * Checks whether infinite source creation is allowed at the given block.
     * Infinite source creation is only allowed in ocean/river biomes at or below sea level.
     * @param block where to test for permission
     * @return whether infinite source creation is allowed
     */
    private boolean isInfiniteSourceAllowed(Block block) {
        if (block.getY() >= 63) return false;

        Biome biome = block.getBiome();
        if (isInfiniteSourceBiome(biome))
            return true;

        for (int x = -BIOME_SEARCH_RANGE; x <= BIOME_SEARCH_RANGE; x += BIOME_SEARCH_STEP)
            for (int z = -BIOME_SEARCH_RANGE; z <= BIOME_SEARCH_RANGE; z += BIOME_SEARCH_STEP)
                if (isInfiniteSourceBiome(block.getRelative(x, 0, z).getBiome()))
                    return true;

        return false;
    }

    /**
     * Checks if the given biome supports infinite sources (ocean or river biomes)
     * @param biome the biome to check
     * @return if the checked biome supports infinite sources
     */
    private boolean isInfiniteSourceBiome(Biome biome) {
        switch (biome) {
            case OCEAN:
            case COLD_OCEAN:
            case DEEP_FROZEN_OCEAN:
            case DEEP_OCEAN:
            case FROZEN_OCEAN:
            case LUKEWARM_OCEAN:
            case WARM_OCEAN:
            case DEEP_COLD_OCEAN:
            case DEEP_LUKEWARM_OCEAN:
            case DEEP_WARM_OCEAN:
            case RIVER:
            case FROZEN_RIVER:
            case BEACH:
            case SNOWY_BEACH:
                return true;

            default:
                return false;
        }
    }

    private static BlockFace[] spreadDirections = new BlockFace[] {
        BlockFace.NORTH,
        BlockFace.EAST,
        BlockFace.SOUTH,
        BlockFace.WEST,
        BlockFace.DOWN
    };

    /**
     * Doesn't prevent source blocks from forming "sometimes". Not exactly sure why but the
     * other listener catches what this one misses, and this one catches what the other one misses.
     * @param evt
     */
    @EventHandler
    public void onFluid(BlockFromToEvent evt) {
        final Block toBlock = evt.getToBlock();

        if (evt.getBlock().getType() != Material.WATER)
            return;

        new BukkitRunnable() {
            @Override
            public void run() {
                if (evt.isCancelled()) return;
                if (isInfiniteSourceAllowed(toBlock)) {
                    // Force source block formation
                    toBlock.setType(Material.WATER);
                    Levelled state = (Levelled) toBlock.getBlockData();
                    state.setLevel(0);
                    toBlock.setBlockData(state);
                    toBlock.getState().update();
                } else {
                    // Prevent source block formation
                    BlockData state = toBlock.getBlockData();
                    if (state instanceof Levelled) {
                        if (((Levelled) state).getLevel() == 0) {
                            ((Levelled) state).setLevel(1);
                            toBlock.setBlockData(state);
                        }
                    }
                }
            }
        }.runTask(plugin);
    }

    /**
     * Doesn't prevent source blocks from forming when a block is destroyed next to two
     * water sources, not sure exactly what causes it but the other listener catches it
     * @param evt
     */
    @EventHandler
    public void onFlow(FluidLevelChangeEvent evt) {
        Block block = evt.getBlock();
        if (block.getType() != Material.WATER)
            return;

        if (isInfiniteSourceAllowed(block)) {
            block.setType(Material.WATER);
            Levelled state = (Levelled) block.getBlockData();
            state.setLevel(0);
            block.setBlockData(state);
            block.getState().update();
            evt.setCancelled(true);
            return;
        }

        BlockData state = evt.getNewData();
        if (state instanceof Levelled) {
            if (((Levelled) state).getLevel() == 0) {
                evt.setCancelled(true);
            }
        }
    }
}
