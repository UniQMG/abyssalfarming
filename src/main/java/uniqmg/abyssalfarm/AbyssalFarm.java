package uniqmg.abyssalfarm;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Hopper;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.abyssalfarm.commands.*;
import uniqmg.abyssalfarm.crop.CropRegistry;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.crops.InvalidCrop;
import uniqmg.abyssalfarm.crop.crops.custom.effects.ParticleProvider;
import uniqmg.abyssalfarm.crop.crops.custom.effects.VisualCropEffect;
import uniqmg.abyssalfarm.crop.crops.custom.drops.InheritedDropHandler;
import uniqmg.abyssalfarm.crop.crops.custom.OverrideCrop;
import uniqmg.abyssalfarm.crop.crops.custom.drops.DropHandler;
import uniqmg.abyssalfarm.crop.crops.custom.drops.ParameterizedDropHandler;
import uniqmg.abyssalfarm.crop.crops.custom.growth.CustomBase;
import uniqmg.abyssalfarm.crop.crops.custom.growth.CustomGrowthProvider;
import uniqmg.abyssalfarm.crop.crops.custom.seed.SeedComparatorBuilder;
import uniqmg.abyssalfarm.crop.world.CropChunk;
import uniqmg.abyssalfarm.crop.world.CropChunkManager;
import uniqmg.abyssalfarm.crop.world.CropData;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.water.NewWaterPhysicsManager;
import uniqmg.abyssalfarm.water.RainBasinFiller;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class AbyssalFarm extends JavaPlugin implements Listener {
    private CropRegistry cropRegistry;
    private CropChunkManager cropChunkManager;
    private RainBasinFiller rainBasinFiller;
    private boolean bailed = false;
    private double greenhouseHeatBonus = 0;
    private double greenhouseWaterMultiplier = 1;

    /**
     * Whether or not fully grown crops still require water to survive.
     */
    private boolean grownCropsRequireWater = true;

    /**
     * The limit to how many blocks can be inspected when determining
     * if a block is a valid basin
     */
    private int rainBasinSearchSize = 0;

    /**
     * The limit to how many blocks crops can inspect while searching for
     * water. This includes non-flowing water blocks inspected.
     */
    private int waterSearchDepth = 0;

    /**
     * How often loaded crop are updated, in ticks
     * Note that crop simulate the actual amount of ticks passed, and
     * this value just controls how often they simulate the passed time.
     */
    private int cropTickRate = 0;

    /**
     * The static instance of the plugin, for convenience.
     */
    public static AbyssalFarm plugin;

    @Override
    public void onEnable() {
        plugin = this;

        ConfigurationSerialization.registerClass(CropChunk.class, "CropChunk");
        ConfigurationSerialization.registerClass(CropData.class, "CropData");
        ConfigurationSerialization.registerClass(CropType.class, "CropType");
        ConfigurationSerialization.registerClass(CropStats.class, "CropStats");

        this.saveDefaultConfig();
        FileConfiguration config = this.getConfig();

        this.cropRegistry = new CropRegistry();
        this.cropRegistry.registerDefaultCrops();

        ConfigurationSection customs = config.getConfigurationSection("crops.custom");
        if (setupCustomCrops(customs)) return;

        this.grownCropsRequireWater = config.getBoolean("options.grownCropsRequireWater");
        this.rainBasinSearchSize = config.getInt("options.rainBasinSearchSize");
        this.waterSearchDepth = config.getInt("options.waterSearchDepth");
        this.cropTickRate = config.getInt("options.cropTickRate");
        this.greenhouseHeatBonus = config.getDouble("options.greenhouseHeatBonus");
        this.greenhouseWaterMultiplier = config.getDouble("options.greenhouseWaterMultiplier");
        this.rainBasinFiller = new RainBasinFiller(this, rainBasinSearchSize);
        this.cropChunkManager = new CropChunkManager(this);

        PluginManager manager = this.getServer().getPluginManager();
        manager.registerEvents(this.cropChunkManager, this);
        manager.registerEvents(this, this);
        manager.registerEvents(new PlantHandler(this), this);
        manager.registerEvents(new HarvestHandler(this), this);
        this.cropRegistry.registerEvents(this);

        if (config.getBoolean("options.disableInfiniteWater"))
            manager.registerEvents(new NewWaterPhysicsManager(this), this);

        if (config.getBoolean("config.hungerDrain"))
            new HungerDrainTask(this).invoke();

        Objects.requireNonNull(getCommand("cropInfo")).setExecutor(new CropInfo(this));
        Objects.requireNonNull(getCommand("testTank")).setExecutor(new TestTank(this));
        Objects.requireNonNull(getCommand("climate")).setExecutor(new Climate());
        Objects.requireNonNull(getCommand("mutate")).setExecutor(new Mutate());
        Objects.requireNonNull(getCommand("test")).setExecutor(new Test());

        new BukkitRunnable() {
            @Override
            public void run() {
                getServer()
                    .getWorlds()
                    .stream()
                    .map(World::getPlayers)
                    .flatMap(Collection::stream)
                    .map(Entity::getLocation)
                    .forEach(location -> {
                        for (int x = -1; x <= 1; x++) {
                            for (int z = -1; z <= 1; z++) {
                                Chunk chunk = location.clone().add(x * 16, 0, z * 16).getChunk();
                                cropChunkManager.getCropChunk(chunk).visualTick();
                            }
                        }
                    });
            }
        }.runTaskTimer(this, 0, 1);

        Logger logger = this.getLogger();
        logger.info("--- Final crop registry ---");
        cropRegistry.getCrops().stream().sorted(Comparator.comparing(CropType::getName)).forEach(cropType -> {
            logger.info(String.format("%s%s%s: %s", ChatColor.GREEN, cropType.getName(), ChatColor.RESET, cropType));
        });
    }

    @EventHandler
    public void onPickup(InventoryPickupItemEvent evt) {
        InventoryHolder holder = evt.getInventory().getHolder();
        if (!(holder instanceof Hopper)) return;

        Hopper hopper = (Hopper) holder;
        Block below = hopper.getBlock().getRelative(0, -1, 0);

        if (below.getType() != Material.SPONGE) return;

        CropStats.clearStack(evt.getItem().getItemStack());
    }

    /**
     * Sets up custom crops
     * @param customs the custom crops config section
     * @return If setting up custom crops failed and bailed
     */
    private boolean setupCustomCrops(ConfigurationSection customs) {
        if (customs != null) {
            Set<String> keys = customs.getKeys(false);

            for (String key: keys) {
                ConfigurationSection section = customs.getConfigurationSection(key);
                assert section != null;

                OverrideCrop custom = createCustom(section, key);
                if (bailed) return true;

                if (custom != null) {
                    this.cropRegistry.overwriteCrop(custom);
                }
            }
        }
        return false;
    }

    /**
     * Bails plugin loading and unloads the plugin after printing an error message
     * @param error The error message to print
     */
    private void bail(String error) {
        getLogger().severe(error);
        if (!bailed) {
            this.getServer().getPluginManager().disablePlugin(this);
            bailed = true;
        }
    }

    /**
     * Creates a custom crop by parsing the given ConfigurationSection
     * @param sect the custom crop ConfigurationSection to parse
     * @param name the name of the custom crop
     * @return the crop, or null if failed and bailed
     */
    private OverrideCrop createCustom(ConfigurationSection sect, String name) {
        CustomGrowthProvider provider = null;
        CropType base;

        if (sect.isString("base")) {
            String baseName = sect.getString("base");
            base = this.cropRegistry.fromName(baseName);

            if (base instanceof InvalidCrop) {
                bail("Unknown base crop: " + baseName);
                return null;
            }
        } else {
            ConfigurationSection baseSect = Objects.requireNonNull(sect.getConfigurationSection("base"));
            provider = CustomBase.fromDef(baseSect);
            base = new InvalidCrop(name);
        }

        OverrideCrop custom = new OverrideCrop(base);
        custom.overrideName(name);

        if (provider != null)
            custom.overrideGrowthProvider(provider);

        /* General stats */
        if (sect.contains("growthTime"))
            custom.overrideMaxGrowth(sect.getInt("growthTime"));

        if (sect.contains("waterUsage"))
            custom.overrideWaterUsage(sect.getDouble("waterUsage"));

        if (sect.contains("temperature"))
            custom.overrideTemperature(sect.getDouble("temperature"));

        if (sect.contains("tolerance"))
            custom.overrideTolerance(sect.getDouble("tolerance"));


        /* Effects */
        if (sect.contains("effects")) {
            ConfigurationSection effects = sect.getConfigurationSection("effects");
            Objects.requireNonNull(effects);

            List<VisualCropEffect> effectList = new ArrayList<>();

            if (effects.contains("particles")) {
                ConfigurationSection particles = effects.getConfigurationSection("particles");
                Objects.requireNonNull(particles);

                for (String particleName: particles.getKeys(false)) {
                    effectList.add(new ParticleProvider(
                        Particle.valueOf(particleName),
                        particles.getInt(particleName + ".count", 1),
                        particles.getInt(particleName + ".interval", 5),
                        particles.getInt(particleName + ".r", 255),
                        particles.getInt(particleName + ".g", 255),
                        particles.getInt(particleName + ".b", 255),
                        particles.getDouble(particleName + ".x", 0.25d),
                        particles.getDouble(particleName + ".y", 0.25d),
                        particles.getDouble(particleName + ".z", 0.25d)
                    ));
                }
            }

            custom.overrideEffects(effectList);
        }

        /* Seeds */
        if (sect.contains("seeds")) {
            ConfigurationSection seeds = sect.getConfigurationSection("seeds");
            Objects.requireNonNull(seeds);

            SeedComparatorBuilder scBuilder = new SeedComparatorBuilder();

            if (seeds.contains("material")) {
                String matName = seeds.getString("material");
                Material mat = Material.valueOf(matName);
                scBuilder.matchMaterial(mat);
            }

            if (seeds.contains("lore")) {
                String lore = seeds.getString("lore");
                Objects.requireNonNull(lore);
                Pattern pattern = Pattern.compile(lore);
                scBuilder.matchLore(pattern);
            }

            if (seeds.contains("disable")) {
                boolean disable = seeds.getBoolean("disable", false);
                if (disable) scBuilder.disable();
            }

            custom.overrideSeeds(scBuilder.build());
        }

        /* Drops */
        if (sect.contains("drops")) {

            List<Map<?, ?>> dropList = sect.getMapList("drops");
            Objects.requireNonNull(dropList);

            List<DropHandler> dropHandlers = dropList
                .stream()
                .map(ParameterizedDropHandler::fromDef)
                .collect(Collectors.toList());

            // Has no effect if drops aren't overriden anyway, so safe to put it here
            if (sect.getBoolean("inheritDrops", false))
                dropHandlers.add(new InheritedDropHandler(custom.getBase()));

            custom.overrideDrops(dropHandlers);
        }

        return custom;
    }

    @Override
    public void onDisable() {
        try {
            if (this.cropChunkManager != null)
                this.cropChunkManager.saveAllChunks();
        } catch (IOException ex) {
            throw new RuntimeException("Failed to save crop chunks", ex);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onTrample(PlayerInteractEvent evt) {
        Block block = evt.getClickedBlock();
        if (block == null) return;
        if (evt.getAction() == Action.PHYSICAL && block.getType() == Material.FARMLAND)
            evt.setCancelled(true);
    }

    @EventHandler
    public void onMoistureChange(MoistureChangeEvent evt) {
        evt.setCancelled(true);
    }

    public RainBasinFiller getRainBasinFiller() {
        return rainBasinFiller;
    }

    public CropChunkManager getCropChunkManager() {
        return this.cropChunkManager;
    }

    public boolean cropsGrownRequireWater() {
        return grownCropsRequireWater;
    }

    public int getRainBasinSearchSize() {
        return rainBasinSearchSize;
    }

    public int getWaterSearchDepth() {
        return waterSearchDepth;
    }

    public int getCropTickRate() {
        return cropTickRate;
    }

    public double getGreenhouseWaterMultiplier() {
        return greenhouseWaterMultiplier;
    }

    public double getGreenhouseHeatBonus() {
        return greenhouseHeatBonus;
    }

    public CropRegistry getCropRegistry() {
        return cropRegistry;
    }
}
