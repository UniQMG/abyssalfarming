package uniqmg.abyssalfarm;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityExplodeEvent;
import uniqmg.abyssalfarm.crop.world.CropChunk;
import uniqmg.abyssalfarm.crop.world.CropData;

public class HarvestHandler implements Listener {
    private AbyssalFarm plugin;

    public HarvestHandler(AbyssalFarm plugin) {
        this.plugin = plugin;
    }

    /**
     * Destroys a crop and drops its' drop items
     * @param target the crop to destroy
     * @return whether the crop was valid and destroyed
     */
    private boolean dropCrop(Block target) {
        CropChunk cropChunk = plugin.getCropChunkManager().getCropChunk(target.getChunk());

        CropData crop = cropChunk.cropAt(target.getLocation());
        if (crop == null || !crop.isValid(target))
            return false;

        cropChunk.breakCrop(target);
        return true;
    }

    @EventHandler(ignoreCancelled = true)
    public void onPhysics(BlockPhysicsEvent evt) {
        Block block = evt.getSourceBlock();
        CropData crop = plugin.getCropChunkManager().getCrop(block.getLocation());
        if (crop != null)
            evt.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onHarvest(BlockBreakEvent evt) {
        if (dropCrop(evt.getBlock()))
            evt.setDropItems(false);

        // Blocks atop this block
        dropCrop(evt.getBlock().getRelative(0, 1, 0));

        // Cocoa blocks on the side of this block
        BlockFace[] cardinalFaces = new BlockFace[] {
            BlockFace.NORTH,
            BlockFace.EAST,
            BlockFace.WEST,
            BlockFace.SOUTH,
        };
        for (BlockFace face: cardinalFaces) {
            Block rel = evt.getBlock().getRelative(face);
            if (rel.getType() == Material.COCOA)
                dropCrop(rel);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onLiquidHarvest(BlockFromToEvent event) {
        if (dropCrop(event.getToBlock()))
            event.setCancelled(true);
    }

    @EventHandler(ignoreCancelled = true)
    public void onPistonHarvest(BlockPistonExtendEvent event) {
        for (Block target: event.getBlocks())
            dropCrop(target);
    }

    @EventHandler(ignoreCancelled = true)
    public void onExplosionHarvest(BlockExplodeEvent event) {
        for (Block block: event.blockList())
            dropCrop(block);
    }

    @EventHandler(ignoreCancelled = true)
    public void onExplosionHarvest(EntityExplodeEvent event) {
        for (Block block: event.blockList())
            dropCrop(block);
    }
}
