package uniqmg.abyssalfarm;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.world.CropChunk;
import uniqmg.abyssalfarm.crop.world.CropData;

import java.util.Objects;

public class PlantHandler implements Listener {
    private AbyssalFarm plugin;

    public PlantHandler(AbyssalFarm plugin) {
        this.plugin = plugin;
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlant(BlockPlaceEvent evt) {
        Block block = evt.getBlockPlaced();
        CropChunk cropChunk = plugin.getCropChunkManager().getCropChunk(block.getChunk());
        cropChunk.initializeCrop(block, evt.getItemInHand());
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlant(PlayerInteractEvent evt) {
        if (evt.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;

        Block block = Objects
            .requireNonNull(evt.getClickedBlock())
            .getRelative(evt.getBlockFace());

        if (!block.getType().isAir())
            return;

        ItemStack item = evt.getItem();
        if (item == null)
            return;

        CropType cropType = plugin.getCropRegistry().fromSeeds(item);
        if (cropType == null)
            return;

        CropChunk cropChunk = plugin.getCropChunkManager().getCropChunk(block.getChunk());
        CropData crop = cropChunk.initializeCrop(block, cropType, CropStats.fromStack(item));
        if (crop == null)
            return;


        item.setAmount(item.getAmount() - 1);
        evt.setCancelled(true);
    }
}
