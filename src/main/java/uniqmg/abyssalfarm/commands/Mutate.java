package uniqmg.abyssalfarm.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;

public class Mutate implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return true;

        ItemStack stack = ((Player) sender).getInventory().getItemInMainHand();
        if (stack.getType().isAir()) return true;

        CropStats cropStats = CropStats.fromStack(stack);
        CropStats.clearStack(stack);

        switch (args.length) {
            case 0:
                CropStats mutated = cropStats.mutate();
                mutated.saveToStack(stack);

                sender.sendMessage(String.format(
                    "Item mutated randomly: [%s%d%s, %s%d%s, %s%d%s, %s%d%s, %s%d%s]",
                    ChatColor.GOLD,
                    mutated.getAdaptability(),
                    ChatColor.RESET,
                    ChatColor.GOLD,
                    mutated.getEfficiency(),
                    ChatColor.RESET,
                    ChatColor.GOLD,
                    mutated.getHardiness(),
                    ChatColor.RESET,
                    ChatColor.GOLD,
                    mutated.getVitality(),
                    ChatColor.RESET,
                    ChatColor.GOLD,
                    mutated.getYield(),
                    ChatColor.RESET
                ));
                return true;

            case 5:
                cropStats.setAdaptability(Integer.valueOf(args[0]));
                cropStats.setEfficiency(Integer.valueOf(args[1]));
                cropStats.setHardiness(Integer.valueOf(args[2]));
                cropStats.setVitality(Integer.valueOf(args[3]));
                cropStats.setYield(Integer.valueOf(args[4]));
                cropStats.saveToStack(stack);
                sender.sendMessage("Item mutated");
                return true;

            default:
                return false;
        }
    }
}
