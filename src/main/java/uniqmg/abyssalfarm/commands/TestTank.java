package uniqmg.abyssalfarm.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import uniqmg.abyssalfarm.AbyssalFarm;

public class TestTank implements CommandExecutor {
    private final AbyssalFarm plugin;

    public TestTank(AbyssalFarm plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return true;

        Location loc = ((Player) sender).getLocation();
        loc = loc.getWorld().getHighestBlockAt(loc).getLocation().add(0, 1, 0);

        // Find highest non-water block
        while(true) {
            Block test = loc.getBlock().getRelative(0, -1, 0);
            if (test.getType() != Material.WATER && !test.getType().isAir()) break;

            loc.add(0, -1, 0);
            if (loc.getY() <= 1) break;
        }

        if (plugin.getRainBasinSearchSize() == 0) {
            sender.sendMessage(ChatColor.YELLOW + "Rain basins are disabled");
            return true;
        }

        if (plugin.getRainBasinFiller().testIsTank(loc.getBlock())) {
            sender.sendMessage(String.format("%sThis is a valid tank! (Y = %d)", ChatColor.GREEN, loc.getBlockY()));
        } else {
            sender.sendMessage(
                String.format(
                    "%sThis is either too large or has holes in the floor and is not a valid tank. The maximum " +
                    "possible tank size is %d blocks in any enclosed shape and must have a solid floor with " +
                    "an optional gap of one block.",
                    ChatColor.RED,
                    plugin.getRainBasinSearchSize()
                )
            );
        }
        return true;
    }
}
