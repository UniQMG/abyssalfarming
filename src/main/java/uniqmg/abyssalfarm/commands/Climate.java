package uniqmg.abyssalfarm.commands;

import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Climate implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return true;
        Block block = ((Player) sender).getLocation().getBlock();
        sender.sendMessage(String.format(
            "Biome: %s. [T %d%%, H %d%%]",
            block.getBiome().name(),
            (int) (block.getTemperature() * 100),
            (int) (block.getHumidity() * 100)
        ));
        return true;
    }
}
