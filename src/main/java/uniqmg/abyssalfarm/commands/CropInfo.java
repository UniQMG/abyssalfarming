package uniqmg.abyssalfarm.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import uniqmg.abyssalfarm.AbyssalFarm;
import uniqmg.abyssalfarm.crop.crops.InvalidCrop;
import uniqmg.abyssalfarm.crop.world.CropChunk;
import uniqmg.abyssalfarm.crop.world.CropData;
import uniqmg.abyssalfarm.crop.world.CropUtil;

public class CropInfo implements CommandExecutor {
    private AbyssalFarm plugin;
    public CropInfo(AbyssalFarm plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player player = (Player) sender;
        Location loc = player.getTargetBlockExact(10).getLocation();

        CropChunk chunk = plugin.getCropChunkManager().getCropChunk(loc.getChunk());
        CropData cropData = chunk.cropAt(loc);

        if (cropData == null) {
            sender.sendMessage(String.format(
                "%sNo crop there. %sAim at a block %sand try again.",
                ChatColor.RED,
                ChatColor.DARK_RED,
                ChatColor.RED
            ));
            return true;
        }

        if (!cropData.isValid(loc.getBlock())) {
            sender.sendMessage(String.format(
                "%sInvalidated crop. %sAim at a valid crop %sand try again.",
                ChatColor.RED,
                ChatColor.DARK_RED,
                ChatColor.RED
            ));
            return true;
        }

        StringBuilder msg = new StringBuilder();
        msg.append(String.format(
            "Crop info: [%s%s%s]\n",

            ChatColor.GREEN,
            cropData.crop.getName(),
            ChatColor.RESET
        ));

        if (cropData.crop instanceof InvalidCrop) {
            msg.append(String.format(
                "%sWarning: %sUnknown crop. Please fix your config.%s\n",
                ChatColor.DARK_RED,
                ChatColor.RED,
                ChatColor.RESET
            ));
            lightLine(loc, msg);
            statsLine(cropData, msg);
        } else if (cropData.crop.isPassive()) {
            msg.append(String.format(
                "%sPassive crop%s: Doesn't grow or die%s\n",
                ChatColor.GOLD,
                ChatColor.YELLOW,
                ChatColor.RESET
            ));
            statsLine(cropData, msg);
        } else {
            growthLine(loc, cropData, msg);
            lightLine(loc, msg);
            temperatureLine(loc, msg);
            healthLine(cropData, msg);
            hydrationLine(cropData, loc, msg);
            statsLine(cropData, msg);
        }

        sender.sendMessage(msg.toString());
        return true;
    }

    private void growthLine(Location loc, CropData cropData, StringBuilder msg) {
        if (!cropData.supportsClimate(loc.getBlock())) {
            if (CropUtil.isOcean(loc.getBlock().getBiome())) {
                msg.append(String.format(
                    "%sCan't grow here. %sOcean biomes%s are too salty to support crops.%s\n",
                    ChatColor.DARK_RED,
                    ChatColor.RED,
                    ChatColor.DARK_RED,
                    ChatColor.RESET
                ));
            } else {
                msg.append(String.format(
                    "%sCan't grow here. Requires temperature %s%d%%±%d%%%s.\n%s",
                    ChatColor.DARK_RED,
                    ChatColor.RED,
                    (int) (cropData.crop.getPreferredTemperature() * 100),
                    (int) ((cropData.crop.getTemperatureTolerance() + cropData.getStats().adaptabilityRange()) * 100),
                    ChatColor.DARK_RED,
                    ChatColor.RESET
                ));
            }
        } else if (!cropData.crop.canGrow(loc.getBlock())) {
            msg.append(String.format(
                "Growth progress: %sAt maximum growth%s\n",
                ChatColor.GREEN,
                ChatColor.RESET
            ));
        } else {
            msg.append(String.format(
                "Growth progress: %s%d%s / %s%d%s\n",

                ChatColor.GREEN,
                cropData.getGrowth(),
                ChatColor.RESET,

                ChatColor.GREEN,
                cropData.crop.getMaxGrowthProgress(),
                ChatColor.RESET
            ));
        }
    }

    private void lightLine(Location loc, StringBuilder msg) {
        byte light = loc.getBlock().getLightFromSky();
        if (light <= 5) {
            msg.append(String.format(
                "%sSky light level: %s%s%s%s\n",
                ChatColor.DARK_RED,
                ChatColor.RED,
                ChatColor.BOLD,
                light,
                ChatColor.RESET
            ));
        } else {
            msg.append(String.format(
                "Sky light level: %s%s%s\n",
                light <= 10
                    ? ChatColor.RED
                    : light < 15
                        ? ChatColor.YELLOW
                        : ChatColor.GREEN,
                light,
                ChatColor.RESET
            ));
        }
    }

    private void temperatureLine(Location loc, StringBuilder msg) {
        boolean isGreenhouse = CropUtil.isGreenhouse(loc.getBlock());
        msg.append(String.format(
            "Temperature: %s%d%%%s%s\n",
            ChatColor.RED,
            (int) (CropUtil.getComputedTemperature(loc.getBlock()) * 100),
            ChatColor.RESET,
            isGreenhouse
                ? String.format(" (%sGreenhouse%s)", ChatColor.GREEN, ChatColor.RESET)
                : ""
        ));
    }

    private void healthLine(CropData cropData, StringBuilder msg) {
        msg.append(String.format(
            "Health: %s%.0f%s / %s%.0f%s\n",

            cropData.getHealth() / cropData.maxHealth() > 0.2
                ? ChatColor.DARK_GREEN
                : ChatColor.DARK_RED,
            cropData.getHealth(),
            ChatColor.RESET,

            ChatColor.DARK_GREEN,
            cropData.maxHealth(),
            ChatColor.RESET
        ));
    }

    private void hydrationLine(CropData cropData, Location loc, StringBuilder msg) {
        boolean isGreenhouse = CropUtil.isGreenhouse(loc.getBlock());

        double waterUsage = cropData.crop.waterUsage() * cropData.getStats().waterUsageMultiplier();
        if (isGreenhouse)
            waterUsage *= plugin.getGreenhouseWaterMultiplier();

        if (waterUsage == 0) {
            msg.append(String.format(
                "Hydration: %sNone required%s\n",
                ChatColor.AQUA,
                ChatColor.RESET
            ));
        } else if (cropData.getHydration() == 0) {
            msg.append(String.format(
                "%sHydration: %s0%smB (%s%s%s/tick)%s\n",
                ChatColor.RED,
                ChatColor.DARK_RED,
                ChatColor.RED,
                ChatColor.AQUA,
                Math.round(waterUsage * 1000d)/1000d,
                ChatColor.DARK_RED,
                ChatColor.RESET
            ));
        } else {
            msg.append(String.format(
                "Hydration: %s%s%smB (%s%s%s/tick)\n",
                ChatColor.AQUA,
                (int) cropData.getHydration(),
                ChatColor.RESET,
                ChatColor.AQUA,
                Math.round(waterUsage * 1000d)/1000d,
                ChatColor.RESET
            ));
        }
    }

    private void statsLine(CropData cropData, StringBuilder msg) {
        msg.append(String.format(
            "[A%s%d%s/E%s%d%s/H%s%d%s/V%s%d%s/Y%s%d%s]",

            ChatColor.GOLD,
            cropData.getStats().getAdaptability(),
            ChatColor.RESET,
            ChatColor.GOLD,
            cropData.getStats().getEfficiency(),
            ChatColor.RESET,
            ChatColor.GOLD,
            cropData.getStats().getHardiness(),
            ChatColor.RESET,
            ChatColor.GOLD,
            cropData.getStats().getVitality(),
            ChatColor.RESET,
            ChatColor.GOLD,
            cropData.getStats().getYield(),
            ChatColor.RESET
        ));
    }
}
