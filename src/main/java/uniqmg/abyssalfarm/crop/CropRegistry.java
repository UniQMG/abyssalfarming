package uniqmg.abyssalfarm.crop;

import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import uniqmg.abyssalfarm.AbyssalFarm;
import uniqmg.abyssalfarm.crop.crops.InvalidCrop;
import uniqmg.abyssalfarm.crop.crops.sapling.Sapling;
import uniqmg.abyssalfarm.crop.crops.sapling.SaplingEventListener;
import uniqmg.abyssalfarm.crop.crops.gourds.GourdHarvestListener;
import uniqmg.abyssalfarm.crop.crops.gourds.Melon;
import uniqmg.abyssalfarm.crop.crops.gourds.Pumpkin;
import uniqmg.abyssalfarm.crop.crops.simple.*;
import uniqmg.abyssalfarm.crop.crops.vertical.Cactus;
import uniqmg.abyssalfarm.crop.crops.vertical.Sugarcane;
import uniqmg.abyssalfarm.crop.crops.vertical.VerticalCropHarvestListener;

import java.util.*;

public class CropRegistry {
    private static final Set<CropType> registrations = new HashSet<>();

    public void registerDefaultCrops() {
        registerCrop(new Wheat());
        registerCrop(new Potato());
        registerCrop(new Carrot());
        registerCrop(new Beetroot());
        registerCrop(new Sugarcane());
        registerCrop(new Cactus());
        registerCrop(new Sweetberry());
        registerCrop(new NetherWart());
        registerCrop(new Melon());
        registerCrop(new Pumpkin());
        registerCrop(new Cocoa());

        registerCrop(new Sapling(Material.OAK_SAPLING, TreeType.TREE, TreeType.BIG_TREE, Material.OAK_LEAVES));
        registerCrop(new Sapling(Material.BIRCH_SAPLING, TreeType.BIRCH, null, Material.BIRCH_LEAVES));
        registerCrop(new Sapling(Material.SPRUCE_SAPLING, TreeType.REDWOOD, TreeType.MEGA_REDWOOD, Material.SPRUCE_LEAVES));
        registerCrop(new Sapling(Material.JUNGLE_SAPLING, TreeType.SMALL_JUNGLE, TreeType.JUNGLE, Material.JUNGLE_LEAVES));
        registerCrop(new Sapling(Material.ACACIA_SAPLING, TreeType.ACACIA, null, Material.ACACIA_LEAVES));
        registerCrop(new Sapling(Material.DARK_OAK_SAPLING, TreeType.DARK_OAK, null, Material.DARK_OAK_LEAVES));
    }

    public void registerEvents(AbyssalFarm plugin) {
        PluginManager manager = plugin.getServer().getPluginManager();
        manager.registerEvents(new VerticalCropHarvestListener(plugin), plugin);
        manager.registerEvents(new SweetberryHarvestListener(plugin), plugin);
        manager.registerEvents(new GourdHarvestListener(plugin), plugin);
        manager.registerEvents(new SaplingEventListener(plugin), plugin);
    }

    private void registerCrop(CropType crop) {
        for (CropType cropType: registrations)
            if (cropType.getName().equals(crop.getName()))
                throw new IllegalStateException("Crop " + cropType.getName() + " already registered");

        for (CropType subCrop: crop.getChildren().values())
            this.registerCrop(subCrop);

        registrations.add(crop);
    }

    public void overwriteCrop(CropType newCrop) {
        Collection<CropType> children = newCrop.getChildren().values();

        registrations.removeIf(existingCrop -> {
            if (newCrop.getName().equals(existingCrop.getName()))
                return true;

            if (children.stream().anyMatch(crop -> crop.getName().equals(existingCrop.getName())))
                return true;

            return false;
        });

        registrations.add(newCrop);
        registrations.addAll(children);
    }

    public Set<CropType> getCrops() {
        return registrations;
    }

    /**
     * Retrieves a registered crop by seeds
     * @param item the ItemStack representing the seeds
     * @return the crop, or null
     */
    public CropType fromSeeds(ItemStack item) {
        for (CropType type: registrations)
            if (type.isValidSeeds(item))
                return type;
        return null;
    }

    /**
     * Retrieves a registered crop by name
     * @param name the name of the crop
     * @return the crop, or an {@link InvalidCrop} placeholder if not found
     */
    public CropType fromName(String name) {
        for (CropType type: registrations)
            if (type.getName().equals(name))
                return type;

        AbyssalFarm.plugin.getLogger().warning("Invalid crop requested: " + name);
        return new InvalidCrop(name);
    }
}
