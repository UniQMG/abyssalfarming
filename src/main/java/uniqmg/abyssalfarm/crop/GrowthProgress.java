package uniqmg.abyssalfarm.crop;

import org.bukkit.block.Block;

public interface GrowthProgress {
    /**
     * Gets the crop's growth progress fraction
     * @return the current growth progress (0 - 1)
     */
    double getGrowthFraction();

    /**
     * Sets the crop's growth progress fraction
     * @param progress The new growth progress (0 - 1)
     */
    void setGrowthFraction(double progress);

    /**
     * Creates a child crop with the given name
     * @param block the block to create the crop in
     * @param name the name of the crop, as set in {@link CropType#getChildren()}
     */
    void createChildCrop(Block block, String name);
}
