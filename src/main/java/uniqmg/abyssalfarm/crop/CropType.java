package uniqmg.abyssalfarm.crop;

import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.AbyssalFarm;

import java.util.*;

/**
 * Represents a static container of behavior and constants associated with a crop
 * All instances should be singletons registered with {@link CropRegistry}
 */
public abstract class CropType implements ConfigurationSerializable  {
    /**
     * Gets the constant and unique name of the CropType
     * @return the name of the crop
     */
    public abstract String getName();

    /**
     * Returns child crops for this crop, which will be registered once this crop is.
     * Crops should be mapped by an internal keyword (and not their actual name) so they
     * can be overridden if necessary. This method should not be called directly from
     * the CropType, instead use {@link GrowthProgress#createChildCrop)} to spawn a
     * crop with a child CropType.
     *
     * @return child crops for this crop, mapped by internal name.
     */
    public Map<String, CropType> getChildren() {
        return Collections.emptyMap();
    }

    /**
     * Checks whether the crop is passive. Passive crops don't receive growth ticks and are excluded
     * from iteration, making them very lightweight.
     * @return Whether the crop is passive.
     */
    public boolean isPassive() {
        return false;
    }

    /**
     * Ticks the crop for a given block when a player is nearby. This should be used for visual effects
     * only, such as particles and animations.
     *
     * @param block The block representing the base of this crop
     * @return how many ticks to wait before the next visual tick. A negative value disables visual ticks.
     */
    public int visualTick(Block block) {
        return -1;
    }

    /**
     * Gets the dropped items
     * @param stats the crop's stats
     * @param progress The fractional growth progress of the crop
     * @return items the crop should drop at its current lifecycle stage
     */
    public abstract List<ItemStack> getDrops(CropStats stats, double progress);

    /**
     * Runs a growth tick, which applies growth progress to the plant.
     * @param block the block to apply growth progress to
     * @param stats the stats of the crop
     * @param growthProgress the growth progress manager
     */
    public abstract void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress);

    /**
     * Get the maximum growth progress for this kind of crop in it's fully grown state
     * @return A number between 0 and 1 representing the growth progress of the crop.
     */
    public abstract int getMaxGrowthProgress();

    /**
     * Checks if the crop is able to grow under the current circumstances. Should be used for considering
     * physical restrictions (e.g. space above sugarcane/cactus, sugarcane/cactus at max height, space for gourd, etc.)
     *
     * If the crop is unable to grow, it won't gain growth progress but will still consume water
     * and receive growth ticks.
     *
     * @param block The block that this crop resides in
     * @return whether the crop is able to grow in its current circumstances
     */
    public abstract boolean canGrow(Block block);

    /**
     * Checks if the crop is valid for a given block. Since blocks can change at any time without the supervision
     * of the plugin, crops are tested for validity before being grown.
     * In most cases, validity implies simply checking if the block's material is the expected material for the crop.
     * @param block the block to test validity on
     * @return whether the crop is valid
     */
    public abstract boolean isValid(Block block);

    /**
     * Plants the crop on the given block. isValid() should return true immediately after
     * if this method succeeds.
     *
     * In certain circumstances (planting a vanilla crop the vanilla way) the crop may
     * already be planted and valid. In this case no action is required.
     *
     * Failure checks should only involve vanilla mechanics and should ignore the actual
     * block that the crop occupies (i.e. no isAir()/!isSolid() checks, only check block
     * below for things like wheat (tilled land) and melon blocks (any solid block)).
     *
     * @param block the block to plant the crop in.
     * @return if planting was successful (ex failure: trying to plant wheat on untilled land)
     */
    public abstract boolean plant(Block block);

    /**
     * Checks if the given item is a valid seed for this crop
     * @param item the item stack
     * @return whether the given ItemStack represents valid seeds for this crop
     */
    public abstract boolean isValidSeeds(ItemStack item);

    /**
     * Gets the preferred temperature of this crop. The crop won't grow outside
     * of this temperature range.
     * @return the center of the preferred temperature range.
     */
    public abstract double getPreferredTemperature();

    /**
     * Gets the temperature tolerance of the crop, which determines how far away it'll grow
     * from its preferred temperature (in either direction). Increased by Adaptability.
     * @return the range of the preferred temperature range.
     */
    public abstract double getTemperatureTolerance();

    /**
     * Gets the water usage of the crop. Returning 0 makes the crop effectively not require water.
     * @return the water usage of the crop, in mB per growth tick
     */
    public abstract double waterUsage();

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("enum", this.getName());
        return map;
    }

    @SuppressWarnings("unused")
    public static CropType deserialize(Map<String, Object> map) {
        if (!map.containsKey("enum")) return null;
        return AbyssalFarm.plugin.getCropRegistry().fromName((String) map.get("enum"));
    }
}
