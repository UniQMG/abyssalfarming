package uniqmg.abyssalfarm.crop;

import org.bukkit.NamespacedKey;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import uniqmg.abyssalfarm.AbyssalFarm;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.bukkit.ChatColor.GOLD;
import static org.bukkit.ChatColor.RESET;

public class CropStats implements ConfigurationSerializable {
    /**
     * Adaptability is the ability for the crop to survive in
     * harsher environments. Widens the range of temperatures
     * and humidities the crop can tolerate.
     *
     * Range: 0-10
     * Baseline: ±0.0 (Depends on crop)
     * Per step: ±0.05
     * Max: ±0.5
     */
    private int adaptability = 0;
    public double adaptabilityRange() {
        return adaptability * 0.05d;
    }
    public int getAdaptability() {
        return adaptability;
    }
    public void setAdaptability(int adaptability) {
        this.adaptability = Math.min(Math.max(adaptability, 0), 10);
    }

    /**
     * Efficiency is the ability for the crop to grow more
     * quickly and with less water.
     *
     * Range: 0-10
     * Baseline: 100% growth time, 100% water usage
     * Per step: -7% growth time, -7% water usage
     * Max: 30% growth time, 30% water usage
     */
    private int efficiency = 0;
    public double growthMultiplier() {
        return 1 / (1 - efficiency * 0.07d);
    }
    public double waterUsageMultiplier() {
        return 1 - efficiency * 0.07d;
    }
    public int getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(int efficiency) {
        this.efficiency = Math.min(Math.max(efficiency, 0), 10);
    }

    /**
     * Hardiness is the ability for the crop to endure physical
     * trauma. Increases trample, flood, and drought resistance
     *
     * Range: 0-10
     * Baseline: 100% damage
     * Per step: -10% damage
     * Max: 0% damage
     */
    private int hardiness = 0;
    public double physicalDamageMultiplier() {
        return 1 - hardiness * 0.1d;
    }
    public int getHardiness() {
        return hardiness;
    }
    public void setHardiness(int hardiness) {
        this.hardiness = Math.min(Math.max(hardiness, 0), 10);
    }

    /**
     * Vitality is the ability for the crop to stay alive in
     * adverse conditions. Increases maximum crop health.
     *
     * Range: 0-10
     * Baseline: 100% health
     * Per step: +40% health
     * Max: 500% health
     */
    private int vitality = 0;
    public double healthMultiplier() {
        return 1 + vitality * 0.4d;
    }
    public int getVitality() {
        return vitality;
    }
    public void setVitality(int vitality) {
        this.vitality = Math.min(Math.max(vitality, 0), 10);
    }

    /**
     * Yield is the ability for the crop to increase its raw
     * material outputs. Increases item drops.
     *
     * Range: 0-10
     * Baseline: 100% drops
     * Per step: +30% drops
     * Max: 400% drops
     */
    private int yield = 0;
    public double yieldMultiplier() {
        return 1 + yield * 0.3d;
    }
    public int getYield() {
        return yield;
    }
    public void setYield(int yield) {
        this.yield = Math.min(Math.max(yield, 0), 10);
    }

    private static NamespacedKey keyAdaptability = new NamespacedKey(AbyssalFarm.plugin, "adaptability");
    private static NamespacedKey keyEfficiency = new NamespacedKey(AbyssalFarm.plugin, "efficiency");
    private static NamespacedKey keyHardiness = new NamespacedKey(AbyssalFarm.plugin, "hardiness");
    private static NamespacedKey keyVitality = new NamespacedKey(AbyssalFarm.plugin, "vitality");
    private static NamespacedKey keyYield = new NamespacedKey(AbyssalFarm.plugin, "yield");

    /**
     * Saves the CropStats to the given ItemStack and adds lore.
     * @param stack the itemstack to save stats to
     */
    public void saveToStack(ItemStack stack) {
        ItemMeta meta = stack.getItemMeta();
        PersistentDataContainer data = meta.getPersistentDataContainer();

        data.set(keyAdaptability, PersistentDataType.INTEGER, this.adaptability);
        data.set(keyEfficiency, PersistentDataType.INTEGER, this.efficiency);
        data.set(keyHardiness, PersistentDataType.INTEGER, this.hardiness);
        data.set(keyVitality, PersistentDataType.INTEGER, this.vitality);
        data.set(keyYield, PersistentDataType.INTEGER, this.yield);

        List<String> lore = new ArrayList<>();
        if (meta.hasLore())
            lore.addAll(meta.getLore());

        lore.add(String.format("%sAdaptability: %s%s", RESET, GOLD, this.adaptability));
        lore.add(String.format("%sEfficiency: %s%s", RESET, GOLD, this.efficiency));
        lore.add(String.format("%sHardiness: %s%s", RESET, GOLD, this.hardiness));
        lore.add(String.format("%sVitality: %s%s", RESET, GOLD, this.vitality));
        lore.add(String.format("%sYield: %s%s", RESET, GOLD, this.yield));

        meta.setLore(lore);
        stack.setItemMeta(meta);
    }

    /**
     * Removes any CropStats from the given ItemStack
     * @param stack the itemstack to clear stats and lore from
     */
    public static void clearStack(ItemStack stack) {
        ItemMeta meta = stack.getItemMeta();
        PersistentDataContainer data = meta.getPersistentDataContainer();

        data.remove(keyAdaptability);
        data.remove(keyEfficiency);
        data.remove(keyHardiness);
        data.remove(keyVitality);
        data.remove(keyYield);

        if (meta.hasLore()) {
            meta.setLore(Objects.requireNonNull(meta.getLore()).stream().filter(s -> {
                if (s.startsWith(String.format("%sAdaptability: %s", RESET, GOLD))) return false;
                if (s.startsWith(String.format("%sEfficiency: %s", RESET, GOLD))) return false;
                if (s.startsWith(String.format("%sHardiness: %s", RESET, GOLD))) return false;
                if (s.startsWith(String.format("%sVitality: %s", RESET, GOLD))) return false;
                if (s.startsWith(String.format("%sYield: %s", RESET, GOLD))) return false;
                return true;
            }).collect(Collectors.toList()));
        }

        stack.setItemMeta(meta);
    }


    /**
     * Mutates the CropStats randomly. Stats are clamped 0-10.
     * Original object is immutable.
     *
     * @return the mutated CropStats
     */
    public CropStats mutate() {
        return this.mutate(0.25, 2.5, 3);
    }

    /**
     * Mutates the CropStats randomly. Stats are clamped 0-10.
     * Original object is immutable.
     * @param baseChance the chance for there to be a mutation, 0 - 1
     * @param posWeight How much the stats can increase
     * @param negWeight How much the stats can decrease
     * @return the mutated CropStats
     */
    public CropStats mutate(double baseChance, double posWeight, double negWeight) {
        CropStats clone = (CropStats) this.clone();
        if (Math.random() > baseChance) {
            // No mutation
            return clone;
        }

        clone.setAdaptability(this.adaptability + rand(posWeight, negWeight));
        clone.setEfficiency(this.efficiency + rand(posWeight, negWeight));
        clone.setHardiness(this.hardiness + rand(posWeight, negWeight));
        clone.setVitality(this.vitality + rand(posWeight, negWeight));
        clone.setYield(this.yield + rand(posWeight, negWeight));
        return clone;
    }

    private int rand(double posWeight, double negWeight) {
        return (int) Math.round(Math.random() * (posWeight + negWeight) - negWeight);
    }

    @Override
    protected Object clone() {
        CropStats clone = new CropStats();
        clone.adaptability = this.adaptability;
        clone.efficiency = this.efficiency;
        clone.hardiness = this.hardiness;
        clone.vitality = this.vitality;
        clone.yield = this.yield;
        return clone;
    }

    /**
     * Generates CropStats previously saved to an ItemStack
     * @param stack the itemstack to load stats from
     * @return the loaded stats
     */
    public static CropStats fromStack(ItemStack stack) {
        ItemMeta meta = stack.getItemMeta();
        PersistentDataContainer data = meta.getPersistentDataContainer();

        CropStats stats = new CropStats();
        stats.adaptability = data.getOrDefault(keyAdaptability, PersistentDataType.INTEGER, 0);
        stats.efficiency = data.getOrDefault(keyEfficiency, PersistentDataType.INTEGER, 0);
        stats.hardiness = data.getOrDefault(keyHardiness, PersistentDataType.INTEGER, 0);
        stats.vitality = data.getOrDefault(keyVitality, PersistentDataType.INTEGER, 0);
        stats.yield = data.getOrDefault(keyYield, PersistentDataType.INTEGER, 0);
        return stats;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
        data.put("adaptability", adaptability);
        data.put("efficiency", efficiency);
        data.put("hardiness", hardiness);
        data.put("vitality", vitality);
        data.put("yield", yield);
        return data;
    }

    @SuppressWarnings("unused")
    public static CropStats deserialize(Map<String, Object> map) {
        CropStats stats = new CropStats();
        stats.adaptability = (int) map.getOrDefault("adaptability", 0);
        stats.efficiency = (int) map.getOrDefault("efficiency", 0);
        stats.hardiness = (int) map.getOrDefault("hardiness", 0);
        stats.vitality = (int) map.getOrDefault("vitality", 0);
        stats.yield = (int) map.getOrDefault("yield", 0);
        return stats;
    }

    @Override
    public String toString() {
        return String.format(
            "CropStats { Adaptability = %s, Efficiency = %s, Hardiness = %s, Vitality = %s, Yield = %s }",
            adaptability,
            efficiency,
            hardiness,
            vitality,
            yield
        );
    }
}
