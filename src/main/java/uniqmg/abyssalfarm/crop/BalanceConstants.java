package uniqmg.abyssalfarm.crop;

public class BalanceConstants {
    /**
     * The global water usage base.
     *
     * 1000/24000 is equal to 1 source block per day and
     * is the standard for most crops at 7 buckets total per crop.
     */
    public static double BASE_WATER_USAGE = 1000d/24000d;

    /**
     * The tree water usage base
     *
     * Equal to 1 source block every 4 days or 9 buckets total per tree.
     */
    public static double TREE_WATER_USAGE = BASE_WATER_USAGE / 4;

    /**
     * Sugarcane water usage
     * Equal to 2 source blocks per day or 14 source blocks total per crop.
     */
    public static double SUGARCANE_WATER_USAGE = BASE_WATER_USAGE * 2;

    /**
     * Cactus water usage
     * Equal to 1 source blocks every 5 days or 1.4 source blocks total per crop
     */
    public static double CACTUS_WATER_USAGE = BASE_WATER_USAGE / 5;

    /**
     * The global growth time base.
     * 168000 is equal to 7 in-game days (2 hours 20 minutes).
     */
    public static int BASE_GROWTH_TIME = 168000;

    /**
     * The tree growth time base
     * 864000 is equal to 36 in-game days (12 hours)
     */
    public static int TREE_GROWTH_TIME = 864000;

}
