package uniqmg.abyssalfarm.crop.crops.vertical;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.GrowthProgress;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class VerticalCrop extends CropType {
    private final Material material;
    VerticalCrop(Material material) {
        this.material = material;
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        // Base sugarcane/cactus always un-mutated
        ItemStack originalSeed = new ItemStack(material);
        originalSeed.setAmount(1);
        stats.saveToStack(originalSeed);

        return Collections.singletonList(originalSeed);
    }

    public Map<String, CropType> getChildren() {
        VerticalCropSegment segment = new VerticalCropSegment(material, this.getName() + " Segment");
        return Collections.singletonMap("segment", segment);
    }

    /**
     * Growth progress is reset whenever the crop is fully grown, and then a new segment crop
     * is spawned above the current segment or base, if possible.
     *
     * @param block the block to apply growth progress to
     * @param stats the stats of the crop at the given block
     * @param growthProgress the growth progress manager
     */
    @Override
    public void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress) {
        if (!isValid(block)) return;

        if (growthProgress.getGrowthFraction() == 1) {
            growthProgress.setGrowthFraction(0);
            Block nextFree = getNextFree(block);
            if (nextFree == null) return;
            growthProgress.createChildCrop(nextFree, "segment");
        }
    }

    @Override
    public boolean canGrow(Block block) {
        return getNextFree(block) != null;
    }

    /**
     * Determines the next free block. A free block is air that is 3 blocks or less above the passed block.
     * @param block the block to start searching from
     * @return the next free block, or null
     */
    private Block getNextFree(Block block) {
        int steps = 1;

        while (block.getType() == material) {
            block = block.getRelative(0, 1, 0);
            if (++steps > 3)
                return null;
        }

        if (block.getType().isAir())
            return block;

        return null;
    }

    @Override
    public boolean isValid(Block block) {
        return block.getType() == material;
    }

    @Override
    public boolean isValidSeeds(ItemStack item) {
        return item.getType() == material;
    }
}
