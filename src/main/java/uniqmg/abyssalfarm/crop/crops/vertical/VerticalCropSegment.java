package uniqmg.abyssalfarm.crop.crops.vertical;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.GrowthProgress;

import java.util.Collections;
import java.util.List;

/**
 * Psuedo-crop representing grown sugarcane or cactus crops above the base crop
 */
public class VerticalCropSegment extends CropType {
    private final Material material;
    private final String name;

    VerticalCropSegment(Material mat, String name) {
        this.material = mat;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        // Upper sugarcane/cactus always mutated
        ItemStack mutatedStack = new ItemStack(material);
        mutatedStack.setAmount((int) stats.yieldMultiplier());
        stats.mutate().saveToStack(mutatedStack);

        return Collections.singletonList(mutatedStack);
    }

    @Override
    public void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress) {
    }

    @Override
    public int getMaxGrowthProgress() {
        return 0;
    }

    @Override
    public boolean isPassive() {
        return true;
    }

    @Override
    public boolean canGrow(Block block) {
        return false;
    }

    @Override
    public boolean isValid(Block block) {
        return block.getType() == material;
    }

    @Override
    public boolean plant(Block block) {
        if (!block.getType().isAir())
            return false;
        block.setType(material);
        return true;
    }

    @Override
    public boolean isValidSeeds(ItemStack item) {
        return false;
    }

    @Override
    public double getPreferredTemperature() {
        return 0;
    }

    @Override
    public double getTemperatureTolerance() {
        return 0;
    }

    @Override
    public double waterUsage() {
        return 0;
    }
}
