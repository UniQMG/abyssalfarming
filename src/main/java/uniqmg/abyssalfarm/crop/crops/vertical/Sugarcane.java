package uniqmg.abyssalfarm.crop.crops.vertical;

import org.bukkit.Material;
import org.bukkit.block.Block;
import uniqmg.abyssalfarm.crop.BalanceConstants;

public class Sugarcane extends VerticalCrop {
    public Sugarcane() {
        super(Material.SUGAR_CANE);
    }

    @Override
    public String getName() {
        return "Sugarcane";
    }

    @Override
    public int getMaxGrowthProgress() {
        return BalanceConstants.BASE_GROWTH_TIME;
    }

    @Override
    public boolean plant(Block block) {
        Material below = block.getRelative(0, -1, 0).getType();
        switch (below) {
            case SAND: case DIRT: case GRASS_BLOCK: case PODZOL: break;
            default: return false;
        }
        block.setType(Material.SUGAR_CANE);
        return true;
    }

    @Override
    public double getPreferredTemperature() {
        return 0.6;
    }

    @Override
    public double getTemperatureTolerance() {
        return 0.2;
    }

    @Override
    public double waterUsage() {
        return BalanceConstants.SUGARCANE_WATER_USAGE;
    }
}
