package uniqmg.abyssalfarm.crop.crops.vertical;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import uniqmg.abyssalfarm.AbyssalFarm;
import uniqmg.abyssalfarm.crop.world.CropChunk;

public class VerticalCropHarvestListener implements Listener {
    private final AbyssalFarm plugin;

    public VerticalCropHarvestListener(AbyssalFarm plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onHarvest(BlockBreakEvent evt) {
        if (evt.isCancelled()) return;
        Block block = evt.getBlock();

        if (block.getType() != Material.SUGAR_CANE && block.getType() != Material.CACTUS)
            return;

        evt.setCancelled(true);

        CropChunk chunk = plugin
            .getCropChunkManager()
            .getCropChunk(block.getLocation().getChunk());

        // Traverse up sugarcane and cactus blocks to prevent vanilla drops
        Block above = block;
        while (above.getType() == Material.SUGAR_CANE || above.getType() == Material.CACTUS) {
            chunk.breakCrop(above);
            above = above.getRelative(0, 1, 0);
        }
    }
}
