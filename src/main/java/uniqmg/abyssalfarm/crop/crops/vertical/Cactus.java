package uniqmg.abyssalfarm.crop.crops.vertical;

import org.bukkit.Material;
import org.bukkit.block.Block;
import uniqmg.abyssalfarm.crop.BalanceConstants;

public class Cactus extends VerticalCrop {
    public Cactus() {
        super(Material.CACTUS);
    }

    @Override
    public String getName() {
        return "Cactus";
    }

    @Override
    public int getMaxGrowthProgress() {
        return BalanceConstants.BASE_GROWTH_TIME;
    }

    @Override
    public boolean plant(Block block) {
        Material below = block.getRelative(0, -1, 0).getType();
        if (below != Material.SAND) return false;
        block.setType(Material.CACTUS);
        return true;
    }

    @Override
    public double getPreferredTemperature() {
        return 1.5;
    }

    @Override
    public double getTemperatureTolerance() {
        return 0.5;
    }

    @Override
    public double waterUsage() {
        return BalanceConstants.CACTUS_WATER_USAGE;
    }
}
