package uniqmg.abyssalfarm.crop.crops.simple;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;

import java.util.ArrayList;
import java.util.List;

public class Beetroot extends SimpleCrop {
    public Beetroot() {
        super(Material.BEETROOTS, Material.BEETROOT_SEEDS, 0.5, 0.25);
    }

    @Override
    public String getName() {
        return "Beetroot";
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();

        if (progress >= 1) {
            ItemStack beets = new ItemStack(Material.BEETROOT);
            beets.setAmount((int) (Math.random() * stats.yieldMultiplier()) + 1);
            drops.add(beets);

            ItemStack seeds = new ItemStack(Material.BEETROOT_SEEDS);
            seeds.setAmount((int) (Math.random() * stats.yieldMultiplier() * 1.2) + 1);
            stats.mutate().saveToStack(seeds);
            drops.add(seeds);
        }

        ItemStack originalSeed = new ItemStack(Material.BEETROOT_SEEDS);
        originalSeed.setAmount(1);
        stats.saveToStack(originalSeed);
        drops.add(originalSeed);
        return drops;
    }
}
