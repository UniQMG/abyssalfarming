package uniqmg.abyssalfarm.crop.crops.simple;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.world.hydration.WaterSearchLocationModifier;

import java.util.ArrayList;
import java.util.List;

public class Cocoa extends SimpleCrop implements WaterSearchLocationModifier {
    public Cocoa() {
        super(Material.COCOA, Material.COCOA_BEANS, 0.9, 0.1);
    }

    @Override
    public String getName() {
        return "Cocoa";
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();

        if (progress >= 1) {
            ItemStack cocoa = new ItemStack(Material.COCOA_BEANS);
            cocoa.setAmount((int) (Math.random() * stats.yieldMultiplier() * 1.2) + 1);
            stats.mutate().saveToStack(cocoa);
            drops.add(cocoa);
        }

        ItemStack original = new ItemStack(Material.COCOA_BEANS);
        original.setAmount(1);
        stats.saveToStack(original);
        drops.add(original);

        return drops;
    }

    @Override
    public boolean plant(Block block) {
        if (block.getType() == Material.COCOA)
            return true;

        if (!block.getType().isAir())
            return false;

        BlockFace[] faces = new BlockFace[] {
            BlockFace.NORTH,
            BlockFace.EAST,
            BlockFace.WEST,
            BlockFace.SOUTH,
        };

        for (BlockFace face: faces) {
            Block rel = block.getRelative(face);
            if (rel.getType() == Material.JUNGLE_LOG) {
                block.setType(Material.COCOA);
                Directional data = (Directional) block.getBlockData();
                data.setFacing(face);
                block.setBlockData(data);
                return true;
            }
        }

        return false;
    }

    @Override
    public Location getInitialLocation(Location start) {
        BlockData data = start.getBlock().getBlockData();
        if (!(data instanceof Directional)) return start;
        Directional dir = (Directional) data;

        Block tree = start.getBlock().getRelative(dir.getFacing());
        while (tree.getType() == Material.JUNGLE_LOG)
            tree = tree.getRelative(0, -1, 0);

        return tree.getLocation();
    }
}
