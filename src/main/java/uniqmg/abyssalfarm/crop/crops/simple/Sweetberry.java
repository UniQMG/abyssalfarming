package uniqmg.abyssalfarm.crop.crops.simple;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Sweetberry extends SimpleCrop {
    public Sweetberry() {
        super(
            Material.SWEET_BERRY_BUSH,
            Material.SWEET_BERRIES,
            0.4,
            0.25,
            Arrays.asList(
                Material.GRASS_BLOCK,
                Material.DIRT,
                Material.FARMLAND
            )
        );
    }

    @Override
    public String getName() {
        return "Sweetberry";
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();

        ItemStack originalSeed = new ItemStack(Material.SWEET_BERRIES);
        originalSeed.setAmount(1);
        stats.saveToStack(originalSeed);
        drops.add(originalSeed);

        drops.addAll(getInteractionItems(stats, progress));

        return drops;
    }

    List<ItemStack> getInteractionItems(CropStats stats, double growthFraction) {
        List<ItemStack> drops = new ArrayList<>();

        if (growthFraction >= 0.5) {
            ItemStack berries = new ItemStack(Material.SWEET_BERRIES);
            double baseMultiplier = Math.random() * stats.yieldMultiplier() * 1.2;
            int amount = (int) (
                growthFraction >= 1
                    ? baseMultiplier * 3
                    : baseMultiplier * 1
            );
            berries.setAmount(amount+1);
            stats.mutate().saveToStack(berries);
            drops.add(berries);
        }

        return drops;
    }
}
