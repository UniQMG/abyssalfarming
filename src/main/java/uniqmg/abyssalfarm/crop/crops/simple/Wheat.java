package uniqmg.abyssalfarm.crop.crops.simple;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;

import java.util.ArrayList;
import java.util.List;

public class Wheat extends SimpleCrop {
    public Wheat() {
        super(Material.WHEAT, Material.WHEAT_SEEDS, 0.6, 0.25);
    }

    @Override
    public String getName() {
        return "Wheat";
    }

    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();

        if (progress >= 1) {
            ItemStack wheat = new ItemStack(Material.WHEAT);
            wheat.setAmount((int) (Math.random() * stats.yieldMultiplier()) + 1);
            drops.add(wheat);

            ItemStack seeds = new ItemStack(Material.WHEAT_SEEDS);
            seeds.setAmount((int) (Math.random() * stats.yieldMultiplier() * 1.2) + 1);
            stats.mutate().saveToStack(seeds);
            drops.add(seeds);
        }

        ItemStack originalSeed = new ItemStack(Material.WHEAT_SEEDS);
        originalSeed.setAmount(1);
        stats.saveToStack(originalSeed);
        drops.add(originalSeed);
        return drops;
    }

}
