package uniqmg.abyssalfarm.crop.crops.simple;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;

import java.util.ArrayList;
import java.util.List;

public class Carrot extends SimpleCrop {
    public Carrot() {
        super(Material.CARROTS, Material.CARROT, 0.4, 0.25);
    }

    @Override
    public String getName() {
        return "Carrot";
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();

        if (progress >= 1) {
            ItemStack seeds = new ItemStack(Material.CARROT);
            seeds.setAmount((int) (Math.random() * stats.yieldMultiplier() * 1.2) + 1);
            stats.mutate().saveToStack(seeds);
            drops.add(seeds);
        }

        ItemStack originalSeed = new ItemStack(Material.CARROT);
        originalSeed.setAmount(1);
        stats.saveToStack(originalSeed);
        drops.add(originalSeed);
        return drops;
    }
}
