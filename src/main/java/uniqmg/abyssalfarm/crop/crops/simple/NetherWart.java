package uniqmg.abyssalfarm.crop.crops.simple;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NetherWart extends SimpleCrop {
    public NetherWart() {
        super(
            Material.NETHER_WART,
            Material.NETHER_WART,
            2.0,
            0.5,
            Collections.singletonList(Material.SOUL_SAND)
        );
    }

    @Override
    public String getName() {
        return "Nether Wart";
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();

        if (progress >= 1) {
            ItemStack seeds = new ItemStack(Material.NETHER_WART);
            seeds.setAmount((int) (Math.random() * stats.yieldMultiplier() * 3) + 1);
            stats.mutate().saveToStack(seeds);
            drops.add(seeds);
        }

        ItemStack originalSeed = new ItemStack(Material.NETHER_WART);
        originalSeed.setAmount(1);
        stats.saveToStack(originalSeed);
        drops.add(originalSeed);
        return drops;
    }

    @Override
    public boolean plant(Block block) {
        Material below = block.getRelative(0, -1, 0).getType();
        if (below != Material.SOUL_SAND) return false;
        block.setType(Material.NETHER_WART);
        return true;
    }

    @Override
    public double waterUsage() {
        return 0;
    }
}
