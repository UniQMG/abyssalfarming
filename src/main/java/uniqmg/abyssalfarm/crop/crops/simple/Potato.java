package uniqmg.abyssalfarm.crop.crops.simple;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;

import java.util.ArrayList;
import java.util.List;

public class Potato extends SimpleCrop {
    public Potato() {
        super(Material.POTATOES, Material.POTATO, -0.2, 0.4);
    }

    @Override
    public String getName() {
        return "Potato";
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();

        if (progress >= 1) {
            ItemStack seeds = new ItemStack(Material.POTATO);
            seeds.setAmount((int) (Math.random() * stats.yieldMultiplier() * 1.2) + 1);
            stats.mutate().saveToStack(seeds);
            drops.add(seeds);
        } else if (progress >= 0.5 && Math.random() > 0.75) {
            ItemStack poison = new ItemStack(Material.POISONOUS_POTATO);
            poison.setAmount((int) (Math.random() * stats.yieldMultiplier() * 1.2) + 1);
            drops.add(poison);
        }

        ItemStack originalSeed = new ItemStack(Material.POTATO);
        originalSeed.setAmount(1);
        stats.saveToStack(originalSeed);
        drops.add(originalSeed);
        return drops;
    }
}
