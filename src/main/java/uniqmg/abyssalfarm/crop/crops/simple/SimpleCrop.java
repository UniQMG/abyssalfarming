package uniqmg.abyssalfarm.crop.crops.simple;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.BalanceConstants;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.GrowthProgress;

import java.util.Collections;
import java.util.List;

/**
 * Represents a simple crop such as wheat, carrots, or potatoes that have
 * specific seeds and items (possibly the same) and grow on a single block
 * of farmland.
 */
public abstract class SimpleCrop extends CropType {
    private final Material crop;
    private final Material seeds;
    private final int growTime;
    private final double temperature;
    private final double tempRange;
    private final List<Material> validPlantingBlocks;

    SimpleCrop(Material crop, Material seeds, double temperature, double tempRange) {
        this(crop, seeds, temperature, tempRange, Collections.singletonList(Material.FARMLAND));
    }

    SimpleCrop(Material crop, Material seeds, double temperature, double tempRange, List<Material> validPlantingBlocks) {
        this.crop = crop;
        this.seeds = seeds;
        this.growTime = BalanceConstants.BASE_GROWTH_TIME;
        this.temperature = temperature;
        this.tempRange = tempRange;
        this.validPlantingBlocks = validPlantingBlocks;
    }

    @Override
    public void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress) {
        BlockData blockData = block.getBlockData();
        if (blockData instanceof Ageable) {
            Ageable ageable = (Ageable) blockData;
            int targetAge = (int) (ageable.getMaximumAge() * growthProgress.getGrowthFraction());

            if (ageable.getAge() != targetAge) {
                ageable.setAge(targetAge);
                block.setBlockData(ageable);
            }
        }
    }

    @Override
    public boolean canGrow(Block block) { return true; }

    @Override
    public boolean isValidSeeds(ItemStack item) {
        return item.getType() == seeds;
    }

    @Override
    public int getMaxGrowthProgress() {
        return this.growTime;
    }

    @Override
    public boolean plant(Block block) {
        Material below = block.getRelative(0, -1, 0).getType();
        if (!validPlantingBlocks.contains(below))
            return false;
        block.setType(crop);
        return true;
    }

    @Override
    public double getPreferredTemperature() {
        return temperature;
    }

    @Override
    public double getTemperatureTolerance() {
        return tempRange;
    }

    @Override
    public boolean isValid(Block block) {
        return block.getType() == crop;
    }

    @Override
    public double waterUsage() {
        return BalanceConstants.BASE_WATER_USAGE;
    }
}