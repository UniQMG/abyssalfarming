package uniqmg.abyssalfarm.crop.crops.simple;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.AbyssalFarm;
import uniqmg.abyssalfarm.crop.world.CropData;

import java.util.List;
import java.util.Objects;

import static org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK;

public class SweetberryHarvestListener implements Listener {
    private final AbyssalFarm plugin;

    public SweetberryHarvestListener(AbyssalFarm plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onHarvest(PlayerInteractEvent evt) {
        if (evt.getAction() != RIGHT_CLICK_BLOCK)
            return;
        Block block = evt.getClickedBlock();
        if (block == null || block.getType() != Material.SWEET_BERRY_BUSH)
            return;

        CropData cropData = plugin.getCropChunkManager().getCrop(block.getLocation());
        if (cropData == null || !(cropData.crop instanceof Sweetberry)) return;

        Sweetberry berryType = (Sweetberry) cropData.crop;
        double growthProgress = cropData.getGrowthFraction();
        List<ItemStack> drops = berryType.getInteractionItems(cropData.getStats(), growthProgress);

        if (drops.size() > 0) {
            Location loc = block.getLocation();
            cropData.setGrowthFraction(0.25);
            for (ItemStack item: drops)
                Objects.requireNonNull(loc.getWorld())
                    .dropItemNaturally(loc, item);
        }

        evt.setCancelled(true);
    }
}
