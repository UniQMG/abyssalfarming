package uniqmg.abyssalfarm.crop.crops.sapling;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.GrowthProgress;

import java.util.ArrayList;
import java.util.List;

/**
 * Psuedo-crop representing leaves in a tree
 */
public class Leaf extends CropType {
    private final Material sapling;
    private final Material leaf;

    public Leaf(Material leaf, Material sapling) {
        this.sapling = sapling;
        this.leaf = leaf;
    }

    @Override
    public String getName() {
        return "Leaf-" + this.leaf.name();
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();
        if (Math.random() <= 1/20d) {
            ItemStack stack = new ItemStack(sapling);
            stats.mutate().saveToStack(stack);
            drops.add(stack);
        }
        return drops;
    }

    @Override
    public boolean isPassive() {
        return true;
    }

    @Override
    public boolean isValid(Block block) {
        return block.getType() == leaf;
    }

    @Override
    public boolean isValidSeeds(ItemStack item) {
        return false;
    }

    @Override
    public void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress) {
    }

    @Override
    public int getMaxGrowthProgress() {
        return 0;
    }

    @Override
    public boolean canGrow(Block block) {
        return false;
    }

    @Override
    public boolean plant(Block block) {
        block.setType(leaf);
        return true;
    }

    @Override
    public double getPreferredTemperature() {
        return 0;
    }

    @Override
    public double getTemperatureTolerance() {
        return 100;
    }

    @Override
    public double waterUsage() {
        return 0;
    }
}
