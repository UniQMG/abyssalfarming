package uniqmg.abyssalfarm.crop.crops.sapling;

import org.bukkit.BlockChangeDelegate;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.abyssalfarm.AbyssalFarm;
import uniqmg.abyssalfarm.crop.GrowthProgress;

class TreeGenBlockChangeDelegate implements BlockChangeDelegate {
    private GrowthProgress progress;
    private final Sapling sapling;
    private final Block block;

    TreeGenBlockChangeDelegate(GrowthProgress progress, Sapling sapling, Block block) {
        this.progress = progress;
        this.sapling = sapling;
        this.block = block;
    }

    @Override
    public boolean setBlockData(int x, int y, int z, BlockData blockData) {
        if (!this.isEmpty(x, y, z)) return false;

        Block target = block.getWorld().getBlockAt(x, y, z);
        target.setBlockData(blockData);

        if (blockData.getMaterial() == sapling.getLeafMaterial())
            progress.createChildCrop(target, "leaf");
        return true;
    }

    @Override
    public BlockData getBlockData(int x, int y, int z) {
        return block.getWorld().getBlockAt(x, y, z).getBlockData();
    }

    @Override
    public int getHeight() {
        return block.getWorld().getMaxHeight(); // ???
    }

    @Override
    public boolean isEmpty(int x, int y, int z) {
        return block.getWorld().getBlockAt(x, y, z).isEmpty();
    }
}
