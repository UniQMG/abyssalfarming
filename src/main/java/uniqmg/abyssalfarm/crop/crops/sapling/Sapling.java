package uniqmg.abyssalfarm.crop.crops.sapling;

import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.BalanceConstants;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.GrowthProgress;

import java.util.*;

public class Sapling extends CropType {
    private final Material sapling;
    private final TreeType tree;
    private final TreeType largeTree;
    private final Material leaf;

    public Sapling(Material sapling, TreeType tree, TreeType largeTree, Material leaf) {
        this.sapling = sapling;
        this.tree = tree;
        this.largeTree = largeTree;
        this.leaf = leaf;
    }

    @Override
    public Map<String, CropType> getChildren() {
        return Collections.singletonMap("leaf", new Leaf(leaf, sapling));
    }

    @Override
    public String getName() {
        return "Sapling-" + this.sapling.name();
    }

    @Override
    public void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress) {
        if (growthProgress.getGrowthFraction() != 1)
            return;

        List<Block> blocks = new ArrayList<>();
        List<BlockData> data = new ArrayList<>();
        blocks.add(block);
        data.add(block.getBlockData());

        BlockFace[][] checks = new BlockFace[][] {
            { BlockFace.NORTH, BlockFace.NORTH_EAST, BlockFace.EAST },
            { BlockFace.NORTH, BlockFace.NORTH_WEST, BlockFace.WEST },
            { BlockFace.SOUTH, BlockFace.SOUTH_EAST, BlockFace.EAST },
            { BlockFace.SOUTH, BlockFace.SOUTH_WEST, BlockFace.WEST },
        };

        checks: for (BlockFace[] check: checks) {
            for (BlockFace face: check) {
                Block rel = block.getRelative(face);
                if (rel.getType() != sapling)
                    continue checks;
            }

            for (BlockFace face: check) {
                Block rel = block.getRelative(face);
                blocks.add(rel);
                data.add(rel.getBlockData());
            }
        }

        // Remove blocks
        blocks.forEach(bl -> block.setType(Material.AIR));

        TreeGenBlockChangeDelegate treeDelegate =
            new TreeGenBlockChangeDelegate(growthProgress, this, block);

        TreeType type = (blocks.size() == 4 && largeTree != null) ? largeTree : tree;

        if (!block.getWorld().generateTree(block.getLocation(), type, treeDelegate))
            // Tree failed to grow, restore the sapling(s)
            for (int i = 0; i < blocks.size(); i++)
                blocks.get(i).setBlockData(data.get(i));
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();
        ItemStack stack = new ItemStack(sapling);
        stats.saveToStack(stack);
        drops.add(stack);
        return drops;
    }

    @Override
    public int getMaxGrowthProgress() {
        return BalanceConstants.TREE_GROWTH_TIME;
    }

    @Override
    public boolean canGrow(Block block) {
        return true;
    }

    @Override
    public boolean plant(Block block) {
        Material below = block.getRelative(0, -1, 0).getType();
        if (!Arrays.asList(Material.GRASS_BLOCK, Material.DIRT, Material.PODZOL, Material.FARMLAND).contains(below))
            return false;
        block.setType(sapling);
        return true;
    }

    @Override
    public boolean isValid(Block block) {
        return block.getType() == sapling;
    }

    @Override
    public boolean isValidSeeds(ItemStack item) {
        return item.getType() == sapling;
    }

    @Override
    public double getPreferredTemperature() {
        return 1;
    }

    @Override
    public double getTemperatureTolerance() {
        return 1;
    }

    @Override
    public double waterUsage() {
        return BalanceConstants.TREE_WATER_USAGE;
    }

    Material getLeafMaterial() {
        return leaf;
    }
}
