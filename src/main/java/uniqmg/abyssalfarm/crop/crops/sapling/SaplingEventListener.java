package uniqmg.abyssalfarm.crop.crops.sapling;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.AbyssalFarm;
import uniqmg.abyssalfarm.crop.world.CropData;

public class SaplingEventListener implements Listener {
    private AbyssalFarm plugin;

    public SaplingEventListener(AbyssalFarm plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onGrow(StructureGrowEvent evt) {
        evt.setCancelled(true);
    }

    @EventHandler
    public void onDecay(LeavesDecayEvent evt) {
        Location location = evt.getBlock().getLocation();
        CropData crop = plugin.getCropChunkManager().getCrop(location);
        if (crop == null) return;

        evt.setCancelled(true);
        evt.getBlock().setType(Material.AIR);
        for (ItemStack drop: crop.getDrops())
            location.getWorld().dropItemNaturally(location, drop);
    }
}
