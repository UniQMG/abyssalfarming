package uniqmg.abyssalfarm.crop.crops;

import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.GrowthProgress;

import java.util.ArrayList;
import java.util.List;

/**
 * Placeholder crop to preserve crops invalidated through config editing mistakes.
 * Doesn't use water, capable of growing anywhere, and has a max growth status of 0.
 * Always valid unless air, has no valid seeds, and doesn't drop anything.
 * At worst you'll reset the growth progress of crops as long as you don't break the crop itself.
 */
public class InvalidCrop extends CropType {
    private final String name;

    public InvalidCrop(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        return new ArrayList<>();
    }

    @Override
    public void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress) {
    }

    @Override
    public int getMaxGrowthProgress() {
        return 0;
    }

    @Override
    public boolean isPassive() {
        return true;
    }

    @Override
    public boolean canGrow(Block block) {
        return false;
    }

    @Override
    public boolean isValid(Block block) {
        return !block.getType().isAir();
    }

    @Override
    public boolean plant(Block block) {
        return false;
    }

    @Override
    public boolean isValidSeeds(ItemStack item) {
        return false;
    }

    @Override
    public double getPreferredTemperature() {
        return 0;
    }

    @Override
    public double getTemperatureTolerance() {
        return 100;
    }

    @Override
    public double waterUsage() {
        return 0;
    }
}
