package uniqmg.abyssalfarm.crop.crops.custom;

import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.GrowthProgress;
import uniqmg.abyssalfarm.crop.crops.custom.drops.DropHandler;
import uniqmg.abyssalfarm.crop.crops.custom.effects.VisualCropEffect;
import uniqmg.abyssalfarm.crop.crops.custom.growth.CustomGrowthProvider;
import uniqmg.abyssalfarm.crop.crops.custom.seed.SeedComparator;

import java.util.*;
import java.util.stream.Collectors;

public class OverrideCrop extends CropType {
    private CropType base;
    public OverrideCrop(CropType base) {
        this.base = base;
    }

    /**
     * Gets the original crop behind this OverrideCrop, even if overridden multiple times
     * @return the original crop
     */
    public CropType getNestedBase() {
        if (base instanceof OverrideCrop)
            return ((OverrideCrop) base).getNestedBase();
        return base;
    }

    /**
     * Gets the crop overridden by this crop. It may be another OverrideCrop.
     * @return the overriden crop
     */
    public CropType getBase() {
        return base;
    }

    private boolean overrideName = false;
    private boolean overrideMaxGrowth = false;
    private boolean overrideTemperature = false;
    private boolean overrideTolerance = false;
    private boolean overrideWaterUsage = false;
    private boolean overrideDrops = false;
    private boolean overrideSeeds = false;
    private boolean overrideEffects = false;
    private boolean overrideGrowth = false;

    private String name = "";
    private int maxGrowth = 0;
    private double temperature = 0;
    private double tolerance = 0;
    private double waterUsage = 0;
    private List<DropHandler> drops;
    private SeedComparator seeds;
    private List<VisualCropEffect> effects;
    private CustomGrowthProvider growthProvider;

    public void overrideName(String name) {
        this.overrideName = true;
        this.name = name;
    }

    public void overrideMaxGrowth(int maxGrowth) {
        this.overrideMaxGrowth = true;
        this.maxGrowth = maxGrowth;
    }

    public void overrideTemperature(double temperature) {
        this.overrideTemperature = true;
        this.temperature = temperature;
    }

    public void overrideTolerance(double tolerance) {
        this.overrideTolerance = true;
        this.tolerance = tolerance;
    }

    public void overrideWaterUsage(double waterUsage) {
        this.overrideWaterUsage = true;
        this.waterUsage = waterUsage;
    }

    public void overrideDrops(List<DropHandler> drops) {
        this.overrideDrops = true;
        this.drops = drops;
    }

    public void overrideSeeds(SeedComparator seeds) {
        this.overrideSeeds = true;
        this.seeds = seeds;
    }

    public void overrideEffects(List<VisualCropEffect> effects) {
        this.overrideEffects = true;
        this.effects = effects;
    }

    public void overrideGrowthProvider(CustomGrowthProvider provider) {
        this.overrideGrowth = true;
        this.growthProvider = provider;
    }

    @Override
    public String getName() {
        if (overrideName)
            return name;
        return base.getName();
    }

    @Override
    public Map<String, CropType> getChildren() {
        if (!this.overrideName)
            return base.getChildren();

        return base
            .getChildren()
            .entrySet()
            .stream()
            .collect(Collectors.toMap(
                Map.Entry::getKey,
                entry -> {
                    OverrideCrop subCrop = new OverrideCrop(entry.getValue());
                    subCrop.overrideName(subCrop.getName() + "-" + this.getName());
                    return subCrop;
                }
            ));
    }

    @Override
    public boolean isPassive() {
        if (overrideGrowth)
            return growthProvider.isPassive();
        return base.isPassive();
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        if (overrideDrops) {
            return drops
                .stream()
                .map(dropHandler -> dropHandler.toStack(stats, progress))
                .filter(Objects::nonNull)
                .flatMap(Collection::stream)
                .filter(itemStack -> itemStack != null && itemStack.getAmount() > 0)
                .collect(Collectors.toList());
        }
        return base.getDrops(stats, progress);
    }

    @Override
    public void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress) {
        if (overrideGrowth) {
            growthProvider.applyGrowthTick(block, stats, growthProgress);
            return;
        }

        base.applyGrowthTick(block, stats, growthProgress);
    }

    @Override
    public int getMaxGrowthProgress() {
        if (overrideMaxGrowth)
            return maxGrowth;
        return base.getMaxGrowthProgress();
    }

    @Override
    public boolean canGrow(Block block) {
        if (overrideGrowth)
            return growthProvider.canGrow(block);
        return base.canGrow(block);
    }

    @Override
    public boolean isValid(Block block) {
        if (overrideGrowth)
            return growthProvider.isValid(block);
        return base.isValid(block);
    }

    @Override
    public boolean plant(Block block) {
        if (overrideGrowth)
            return growthProvider.plant(block);
        return base.plant(block);
    }

    @Override
    public boolean isValidSeeds(ItemStack item) {
        if (overrideSeeds)
            return seeds.isValidSeeds(item);
        return base.isValidSeeds(item);
    }

    @Override
    public double getPreferredTemperature() {
        if (overrideTemperature)
            return temperature;
        return base.getPreferredTemperature();
    }

    @Override
    public double getTemperatureTolerance() {
        if (overrideTolerance)
            return tolerance;
        return base.getTemperatureTolerance();
    }

    @Override
    public double waterUsage() {
        if (overrideWaterUsage)
            return waterUsage;
        return base.waterUsage();
    }

    @Override
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    public int visualTick(Block block) {
        if (overrideEffects) {
            if (effects.size() == 0) return -1;
            final long time = block.getWorld().getFullTime();

            for (VisualCropEffect effect: this.effects)
                if (time % effect.getInterval() == 0)
                    effect.invoke(block);

            int minInterval = effects.stream().mapToInt(VisualCropEffect::getInterval).min().getAsInt();
            return minInterval - (int) (time % minInterval);
        }
        return base.visualTick(block);
    }

//    public static void debugCrop(CropType crop, String indent) {
//        if (crop instanceof OverrideCrop) {
//            List<String> features = new ArrayList<>();
//            OverrideCrop oc = (OverrideCrop) crop;
//            if (oc.overrideName) features.add("name");
//            if (oc.overrideMaxGrowth) features.add("growth");
//            if (oc.overrideTemperature) features.add("temp");
//            if (oc.overrideTolerance) features.add("tol");
//            if (oc.overrideWaterUsage) features.add("water");
//            if (oc.overrideDrops) features.add("drops");
//            if (oc.overrideSeeds) features.add("seeds");
//            if (oc.overrideEffects) features.add("fx");
//            System.out.printf("%s%s: %s %s%n", indent, crop.getClass().getSimpleName(), crop.getName(), features);
//            debugCrop(((OverrideCrop) crop).getBase(), "  " + indent);
//        } else {
//            System.out.printf("%s%s: %s%n", indent, crop.getClass().getSimpleName(), crop.getName());
//        }
//    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("OverrideCrop { ");

        if (overrideName)
            str.append("name = ").append(name).append(", ");
        if (overrideGrowth)
            str.append("overrideGrowth = ").append(overrideGrowth).append(", ");
        if (overrideMaxGrowth)
            str.append("maxGrowth = ").append(maxGrowth).append(", ");
        if (overrideTemperature)
            str.append("temperature = ").append(temperature).append(", ");
        if (overrideTolerance)
            str.append("tolerance = ").append(tolerance).append(", ");
        if (overrideWaterUsage)
            str.append("waterUsage = ").append(waterUsage).append(", ");
        if (overrideDrops)
            str.append("drops = ").append(drops).append(", ");
        if (overrideSeeds)
            str.append("seeds = ").append(seeds).append(", ");

        str.append("base = ").append(base).append(", ");

        // Remove last "," but not the space after it
        str.replace(str.length()-2, str.length()-1, "");
        str.append("}");

        return str.toString();
    }
}
