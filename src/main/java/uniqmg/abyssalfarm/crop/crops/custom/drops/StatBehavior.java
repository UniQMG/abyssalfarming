package uniqmg.abyssalfarm.crop.crops.custom.drops;

enum StatBehavior {
    IGNORE, INHERIT, MUTATE
}
