package uniqmg.abyssalfarm.crop.crops.custom.drops;

import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;

import java.util.List;

public interface DropHandler {
    List<ItemStack> toStack(CropStats parentStats, double growth);
}
