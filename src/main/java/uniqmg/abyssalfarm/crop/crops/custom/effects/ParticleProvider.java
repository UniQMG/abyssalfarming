package uniqmg.abyssalfarm.crop.crops.custom.effects;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.Objects;

public class ParticleProvider implements VisualCropEffect {
    private final Particle particle;
    /**
     * The number of particles spawned
     */
    private final int count;
    /**
     * Ticks between spawning particles
     */
    private final int interval;
    /**
     * Red-value of the particle (REDSTONE and SPELL_MOB only)
     */
    private final int r;
    /**
     * Green-value of the particle (REDSTONE and SPELL_MOB only)
     */
    private final int g;
    /**
     * Blue-value of the particle (REDSTONE and SPELL_MOB only)
     */
    private final int b;
    /**
     * offsetX value of the particle (except SPELL_MOB)
     */
    private final double offsetX;
    /**
     * offsetY value of the particle (except SPELL_MOB)
     */
    private final double offsetY;
    /**
     * offsetZ value of the particle (except SPELL_MOB)
     */
    private final double offsetZ;

    public ParticleProvider(
        Particle particle,
        int count,
        int interval,
        int r,
        int g,
        int b,
        double offsetX,
        double offsetY,
        double offsetZ
    ) {
        this.count = count;
        this.interval = interval;
        this.particle = particle;
        this.r = r;
        this.g = g;
        this.b = b;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.offsetZ = offsetZ;
    }

    public void spawn(Location loc) {
        World world = Objects.requireNonNull(loc.getWorld());

        switch (particle) {
            case REDSTONE:
                Particle.DustOptions dustOptions = new Particle.DustOptions(Color.fromRGB(r, g, b), 1);
                world.spawnParticle(Particle.REDSTONE, loc, count, offsetX, offsetY, offsetZ, dustOptions);
                break;

            case SPELL_MOB:
                world.spawnParticle(
                    Particle.SPELL_MOB,
                    loc,
                    0,
                    r/255D,
                    g/255D,
                    b/255d,
                    1
                );
                break;

            default:
                world.spawnParticle(particle, loc, count, offsetX, offsetY, offsetZ);
                break;
        }
    }

    @Override
    public int getInterval() {
        return interval;
    }

    @Override
    public void invoke(Block block) {
        this.spawn(block.getLocation().add(0.5, 0.5, 0.5));
    }
}
