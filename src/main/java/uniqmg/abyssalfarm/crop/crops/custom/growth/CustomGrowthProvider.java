package uniqmg.abyssalfarm.crop.crops.custom.growth;

import org.bukkit.block.Block;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.GrowthProgress;

public interface CustomGrowthProvider {
    void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress);

    boolean canGrow(Block block);

    boolean isValid(Block block);

    boolean plant(Block block);

    boolean isPassive();
}
