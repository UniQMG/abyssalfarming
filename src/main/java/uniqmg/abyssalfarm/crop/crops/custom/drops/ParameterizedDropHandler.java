package uniqmg.abyssalfarm.crop.crops.custom.drops;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import uniqmg.abyssalfarm.crop.CropStats;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ParameterizedDropHandler implements DropHandler {
    private final Material material;
    private final String name;
    private final String lore;
    private final StatBehavior statBehavior;
    private final double growth;

    private double chance;
    private final double minQuantity;
    private final double maxQuantity;
    private boolean yieldMultiply;

    // Stat ranges
    private final int minAdaptability;
    private final int minEfficiency;
    private final int minHardiness;
    private final int minVitality;
    private final int minYield;
    private final int maxAdaptability;
    private final int maxEfficiency;
    private final int maxHardiness;
    private final int maxVitality;
    private final int maxYield;

    private ParameterizedDropHandler(
        Material material,
        String name,
        String lore,
        StatBehavior statBehavior,
        double growth,

        double chance,
        double minQuantity,
        double maxQuantity,
        boolean yieldMultiply,

        int minAdaptability,
        int minEfficiency,
        int minHardiness,
        int minVitality,
        int minYield,

        int maxAdaptability,
        int maxEfficiency,
        int maxHardiness,
        int maxVitality,
        int maxYield
    ) {
        this.material = material;
        this.name = name;
        this.lore = lore;
        this.statBehavior = statBehavior;
        this.growth = growth;

        this.chance = chance;
        this.minQuantity = minQuantity;
        this.maxQuantity = maxQuantity;
        this.yieldMultiply = yieldMultiply;

        this.minAdaptability = minAdaptability;
        this.minEfficiency = minEfficiency;
        this.minHardiness = minHardiness;
        this.minVitality = minVitality;
        this.minYield = minYield;

        this.maxAdaptability = maxAdaptability;
        this.maxEfficiency = maxEfficiency;
        this.maxHardiness = maxHardiness;
        this.maxVitality = maxVitality;
        this.maxYield = maxYield;
    }

    @SuppressWarnings("unchecked")
    public static DropHandler fromDef(Map data) {
        // Todo: This entire method is probably the unsafest thing since raw pointers.

        double minQuantity, maxQuantity;
        if (data.containsKey("quantity")) {
            minQuantity = ((Number) data.getOrDefault("quantity", 1d)).doubleValue();
            maxQuantity = minQuantity;
        } else {
            minQuantity = ((Number) data.getOrDefault("minQuantity", 1d)).doubleValue();
            maxQuantity = ((Number) data.getOrDefault("maxQuantity", 1d)).doubleValue();
        }

        return new ParameterizedDropHandler(
            Material.valueOf(((String) data.get("material")).toUpperCase()),
            (String) data.getOrDefault("name", ""),
            (String) data.getOrDefault("lore", ""),
            StatBehavior.valueOf(((String) data.getOrDefault("stats", "ignore")).toUpperCase()),
            ((Number) data.getOrDefault("growth", 1)).doubleValue(),
            ((Number) data.getOrDefault("chance", 1)).doubleValue(),
            minQuantity,
            maxQuantity,
            (boolean) data.getOrDefault("yieldMultiply", false),

            ((Number) data.getOrDefault("minAdaptability", 0)).intValue(),
            ((Number) data.getOrDefault("minEfficiency", 0)).intValue(),
            ((Number) data.getOrDefault("minHardiness", 0)).intValue(),
            ((Number) data.getOrDefault("minVitality", 0)).intValue(),
            ((Number) data.getOrDefault("minYield", 0)).intValue(),

            ((Number) data.getOrDefault("maxAdaptability", 10)).intValue(),
            ((Number) data.getOrDefault("maxEfficiency", 10)).intValue(),
            ((Number) data.getOrDefault("maxHardiness", 10)).intValue(),
            ((Number) data.getOrDefault("maxVitality", 10)).intValue(),
            ((Number) data.getOrDefault("maxYield", 10)).intValue()
        );
    }

    /**
     * Creates an ItemStack for the DropHelper
     * @param parentStats The stats of the parent crop
     * @param growth The growth of the parent crop
     * @return the ItemStacks, or null if the proper growth wasn't reached
     */
    @Override
    public List<ItemStack> toStack(CropStats parentStats, double growth) {
        if (growth < this.growth) return null;
        if (Math.random() > this.chance) return null;

        if (parentStats.getAdaptability() > maxAdaptability) return null;
        if (parentStats.getEfficiency() > maxEfficiency) return null;
        if (parentStats.getHardiness() > maxHardiness) return null;
        if (parentStats.getVitality() > maxVitality) return null;
        if (parentStats.getYield() > maxYield) return null;
        if (parentStats.getAdaptability() < minAdaptability) return null;
        if (parentStats.getEfficiency() < minEfficiency) return null;
        if (parentStats.getHardiness() < minHardiness) return null;
        if (parentStats.getVitality() < minVitality) return null;
        if (parentStats.getYield() < minYield) return null;

        ItemStack stack = new ItemStack(material);

        double baseQuantity = Math.random() * (maxQuantity - minQuantity) + minQuantity;
        if (yieldMultiply)
            baseQuantity *= parentStats.yieldMultiplier();
        stack.setAmount((int) Math.round(baseQuantity));

        ItemMeta itemMeta = stack.getItemMeta();
        assert itemMeta != null;
        if (name.length() > 0)
            itemMeta.setDisplayName(name);
        if (lore.length() > 0)
            itemMeta.setLore(Arrays.asList(lore.split("\n")));
        stack.setItemMeta(itemMeta);

        switch (statBehavior) {
            case IGNORE:
                break;
            case INHERIT:
                parentStats.saveToStack(stack);
                break;
            case MUTATE:
                parentStats.mutate().saveToStack(stack);
                break;
        }

        return Collections.singletonList(stack);
    }

    @Override
    public String toString() {
        return String.format(
            "DropHelper { material = %s, lore = %s, minQuantity = %s, maxQuantity = %s, " +
            "chance = %s, yieldMultiply = %s, growth = %s, statBehavior = %s }",
            material,
            lore,
            minQuantity,
            maxQuantity,
            chance,
            yieldMultiply,
            growth,
            statBehavior
        );
    }
}
