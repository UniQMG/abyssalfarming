package uniqmg.abyssalfarm.crop.crops.custom.growth;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.ConfigurationSection;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.GrowthProgress;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CustomBase implements CustomGrowthProvider {
    private final List<Material> stages;
    private final Material soil;

    private CustomBase(List<Material> stages, Material soil) {
        this.stages = stages;
        this.soil = soil;
    }

    public static CustomBase fromDef(ConfigurationSection base) {
        List<Material> stages = Objects.requireNonNull(base.getStringList("stages"))
            .stream()
            .map(Material::valueOf)
            .collect(Collectors.toList());

        if (stages.size() == 0)
            throw new RuntimeException("Cannot have a CustomBase with no stages");

        Material soil = Material.valueOf(base.getString("soil", "AIR"));
        if (soil == Material.AIR) soil = null;

        return new CustomBase(stages, soil);
    }


    @Override
    public void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress) {
        double stage = growthProgress.getGrowthFraction() * (stages.size()-1);
        Material mat = stages.get((int) stage);

        if (block.getType() != mat)
            block.setType(mat);

        BlockData data = block.getBlockData();
        if (data instanceof Ageable) {
            Ageable age = (Ageable) data;
            int targetAge = (int) (Math.round((stage % 1d) * age.getMaximumAge()));
            if (age.getAge() != targetAge) {
                age.setAge(targetAge);
                block.setBlockData(age);
            }
        }
    }

    @Override
    public boolean canGrow(Block block) {
        return true;
    }

    @Override
    public boolean isValid(Block block) {
        return stages.contains(block.getType());
    }

    @Override
    public boolean plant(Block block) {
        if (this.soil != null)
            if (block.getRelative(0, -1, 0).getType() != this.soil)
                return false;

        if (!block.getType().isAir())
            return false;

        block.setType(stages.get(0));
        return true;
    }

    @Override
    public boolean isPassive() {
        return false;
    }
}
