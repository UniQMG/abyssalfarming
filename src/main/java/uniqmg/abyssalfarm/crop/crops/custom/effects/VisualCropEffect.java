package uniqmg.abyssalfarm.crop.crops.custom.effects;

import org.bukkit.block.Block;

/**
 * Defines an effect for an OverrideCrop
 */
public interface VisualCropEffect {
    /**
     * Invokes the effect on the given block. May be called sooner than requested
     * @param block the block to invoke the effect on
     */
    void invoke(Block block);

    /**
     * Gets the interval between running this visual effect
     * @return the interval between running this visual effect
     */
    int getInterval();
}
