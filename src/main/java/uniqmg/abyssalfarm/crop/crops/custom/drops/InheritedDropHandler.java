package uniqmg.abyssalfarm.crop.crops.custom.drops;

import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;

import java.util.List;

public class InheritedDropHandler implements DropHandler {
    private CropType type;

    public InheritedDropHandler(CropType type) {
        this.type = type;
    }

    @Override
    public List<ItemStack> toStack(CropStats parentStats, double growth) {
        return type.getDrops(parentStats, growth);
    }
}
