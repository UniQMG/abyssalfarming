package uniqmg.abyssalfarm.crop.crops.custom.seed;

import org.bukkit.Material;

import java.util.regex.Pattern;

public class SeedComparatorBuilder {
    private SeedComparator target;
    private boolean finalized;

    public SeedComparatorBuilder() {
        this.target = new SeedComparator();
    }

    public void matchMaterial(Material mat) {
        testFinalized();
        target.material = mat;
    }

    public void matchLore(Pattern lore) {
        testFinalized();
        target.lore = lore;
    }

    public void disable() {
        testFinalized();
        target.disabled = true;
    }

    private void testFinalized() {
        if (finalized)
            throw new IllegalStateException("Already finalized");
    }

    public SeedComparator build() {
        finalized = true;
        return target;
    }
}
