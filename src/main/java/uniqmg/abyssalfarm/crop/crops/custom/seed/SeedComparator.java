package uniqmg.abyssalfarm.crop.crops.custom.seed;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class SeedComparator {
    /**
     * A material that should match
     */
    Material material = null;
    /**
     * A regex that should match the given lore
     */
    Pattern lore = null;
    /**
     * If this SeedComparator matches nothing
     */
    boolean disabled = false;

    SeedComparator() { /* Package private */ }

    public boolean isValidSeeds(ItemStack seeds) {
        if (this.disabled) return false;
        if (seeds.getType() != material) return false;

        ItemMeta meta = seeds.getItemMeta();
        assert meta != null;

        StringBuilder lore = new StringBuilder();
        List<String> loreLines = meta.hasLore()
            ? meta.getLore()
            : new ArrayList<>();
        assert loreLines != null;

        for (String str: loreLines) {
            lore.append(str);
            lore.append("\n");
        }

        if (this.lore != null && !this.lore.matcher(lore).find()) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return String.format(
            "SeedComparator { material = %s, lore = %s }",
            material,
            lore
        );
    }
}
