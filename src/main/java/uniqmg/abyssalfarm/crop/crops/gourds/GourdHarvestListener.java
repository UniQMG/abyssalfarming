package uniqmg.abyssalfarm.crop.crops.gourds;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Directional;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.AbyssalFarm;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.crops.custom.OverrideCrop;
import uniqmg.abyssalfarm.crop.world.CropData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GourdHarvestListener implements Listener {
    private final AbyssalFarm plugin;

    public GourdHarvestListener(AbyssalFarm plugin) {
        this.plugin = plugin;
    }

    /**
     * When crafting seeds, inherit CropStats
     * @param event the item craft event
     */
    @EventHandler
    public void onCraftSeeds(CraftItemEvent event) {
        CraftingInventory inv = event.getInventory();
        ItemStack result = inv.getResult();
        if (result == null) return;

        result = result.clone();
        if (
            result.getType() != Material.MELON_SEEDS &&
            result.getType() != Material.PUMPKIN_SEEDS
        ) return;

        for (ItemStack item: inv.getMatrix()) {
            if (item == null) continue;

            // Sanity check
            assert item.getType() == Material.MELON_SLICE
                || item.getType() == Material.PUMPKIN;

            CropStats.fromStack(item).saveToStack(result);
            int multiplier = item.getType() == Material.PUMPKIN ? 4 : 1;
            result.setAmount(item.getAmount() * multiplier);
            inv.remove(item);

            inv.setResult(result);
        }
    }

    /**
     * Reduces growth on gourd stems when the gourd is harvested
     * TODO: Consider removing this since the gourd can now self-reset growth progress
     * @param evt the block break event
     */
    @EventHandler
    public void onHarvest(BlockBreakEvent evt) {
        List<BlockFace> faces = new ArrayList<>(Arrays.asList(
            BlockFace.NORTH,
            BlockFace.SOUTH,
            BlockFace.EAST,
            BlockFace.WEST
        ));

        Block block = evt.getBlock();
        if (block.getType() == Material.MELON || block.getType() == Material.PUMPKIN) {
            for (BlockFace face: faces) {
                Block rel = block.getRelative(face);
                if (rel.getType() == Material.ATTACHED_MELON_STEM ||
                    rel.getType() == Material.ATTACHED_PUMPKIN_STEM) {
                    Directional dir = (Directional) rel.getBlockData();

                    if (dir.getFacing() == face.getOppositeFace()) {
                        CropData cropData = plugin
                            .getCropChunkManager()
                            .getCrop(rel.getLocation());

                        CropType cropType = cropData.crop instanceof OverrideCrop
                            ? ((OverrideCrop) cropData.crop).getNestedBase()
                            : cropData.crop;

                        if (cropType instanceof GourdStem) {
                            cropData.setGrowthFraction(((GourdStem) cropType).getStemMaxGrowthFraction());
                        }
                    }
                }
            }
        }
    }
}
