package uniqmg.abyssalfarm.crop.crops.gourds;

import org.bukkit.Material;
import uniqmg.abyssalfarm.crop.BalanceConstants;

public class Pumpkin extends GourdStem {
    public Pumpkin() {
        super(
            new PumpkinBlock(),
            Material.PUMPKIN_STEM,
            Material.ATTACHED_PUMPKIN_STEM,
            Material.PUMPKIN_SEEDS
        );
    }

    @Override
    public String getName() {
        return "Pumpkin";
    }

    @Override
    public int getMaxGrowthProgress() {
        return BalanceConstants.BASE_GROWTH_TIME;
    }

    @Override
    protected double getStemMaxGrowthFraction() {
        return 0.5;
    }

    @Override
    public double getPreferredTemperature() {
        return 0.4;
    }

    @Override
    public double getTemperatureTolerance() {
        return 0.2;
    }
}
