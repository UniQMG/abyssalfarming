package uniqmg.abyssalfarm.crop.crops.gourds;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;

import java.util.ArrayList;
import java.util.List;

public class MelonBlock extends GourdBlock {
    public MelonBlock() {
        super(Material.MELON);
    }

    @Override
    public String getName() {
        return "Melon Block";
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();

        ItemStack melons = new ItemStack(Material.MELON_SLICE);
        melons.setAmount((int) (1 + (3 * stats.yieldMultiplier() * Math.random())));
        stats.mutate().saveToStack(melons);
        drops.add(melons);

        return drops;
    }
}
