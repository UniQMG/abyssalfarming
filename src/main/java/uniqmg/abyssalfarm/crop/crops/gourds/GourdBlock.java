package uniqmg.abyssalfarm.crop.crops.gourds;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.GrowthProgress;

/**
 * Pseudo-crop representing melon and pumpkin blocks
 */
public abstract class GourdBlock extends CropType {
    private Material block;

    public GourdBlock(Material block) {
        this.block = block;
    }

    @Override
    public boolean isPassive() {
        return true;
    }

    @Override
    public void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress) {
    }

    @Override
    public int getMaxGrowthProgress() {
        return 0;
    }

    @Override
    public boolean canGrow(Block block) {
        return false;
    }

    @Override
    public boolean isValid(Block block) {
        return block.getType() == this.block;
    }

    @Override
    public boolean plant(Block block) {
        if (!block.getRelative(0, -1, 0).getType().isSolid())
            return false;
        block.setType(this.block);
        return true;
    }

    @Override
    public boolean isValidSeeds(ItemStack item) {
        return false;
    }

    @Override
    public double getPreferredTemperature() {
        return 0;
    }

    @Override
    public double getTemperatureTolerance() {
        return 0;
    }

    @Override
    public double waterUsage() {
        return 0;
    }
}
