package uniqmg.abyssalfarm.crop.crops.gourds;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import uniqmg.abyssalfarm.crop.CropStats;

import java.util.ArrayList;
import java.util.List;

import static org.bukkit.Material.PUMPKIN;

public class PumpkinBlock extends GourdBlock {
    public PumpkinBlock() {
        super(PUMPKIN);
    }

    @Override
    public String getName() {
        return "Pumpkin Block";
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();

        ItemStack pumpkins = new ItemStack(PUMPKIN);
        pumpkins.setAmount((int) Math.max(1, stats.yieldMultiplier() * Math.random()));
        stats.mutate().saveToStack(pumpkins);

        ItemMeta meta = pumpkins.getItemMeta();
        assert meta != null;

        List<String> lore = meta.hasLore() ? meta.getLore() : new ArrayList<>();
        assert lore != null;

        lore.add(0, String.format(
            "%sCrop properties will be lost if placed",
            ChatColor.RED
        ));
        meta.setLore(lore);
        pumpkins.setItemMeta(meta);

        drops.add(pumpkins);

        return drops;
    }
}
