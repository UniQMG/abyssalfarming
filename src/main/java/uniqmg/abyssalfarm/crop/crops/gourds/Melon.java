package uniqmg.abyssalfarm.crop.crops.gourds;

import org.bukkit.Material;
import uniqmg.abyssalfarm.crop.BalanceConstants;

public class Melon extends GourdStem {
    public Melon() {
        super(
            new MelonBlock(),
            Material.MELON_STEM,
            Material.ATTACHED_MELON_STEM,
            Material.MELON_SEEDS
        );
    }

    @Override
    public String getName() {
        return "Melon";
    }

    @Override
    public int getMaxGrowthProgress() {
        return BalanceConstants.BASE_GROWTH_TIME;
    }

    @Override
    protected double getStemMaxGrowthFraction() {
        return 0.5d;
    }

    @Override
    public double getPreferredTemperature() {
        return 0.7;
    }

    @Override
    public double getTemperatureTolerance() {
        return 0.2;
    }
}
