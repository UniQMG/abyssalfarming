package uniqmg.abyssalfarm.crop.crops.gourds;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.Directional;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.crop.BalanceConstants;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.GrowthProgress;

import java.util.*;

public abstract class GourdStem extends CropType {
    private final GourdBlock gourdCrop;
    private final Material growing_stem;
    private final Material attached_stem;
    private final Material seeds;

    GourdStem(GourdBlock gourdCrop, Material growing_stem, Material attached_stem, Material seeds) {
        this.gourdCrop = gourdCrop;
        this.growing_stem = growing_stem;
        this.attached_stem = attached_stem;
        this.seeds = seeds;
    }

    @Override
    public Map<String, CropType> getChildren() {
        return Collections.singletonMap("gourd", gourdCrop);
    }

    @Override
    public List<ItemStack> getDrops(CropStats stats, double progress) {
        List<ItemStack> drops = new ArrayList<>();

        if (progress > this.getStemMaxGrowthFraction()/this.getMaxGrowthProgress()) {
            ItemStack seeds = new ItemStack(this.seeds);
            seeds.setAmount((int) (2 * Math.random() * stats.yieldMultiplier()));
            stats.mutate().saveToStack(seeds);
            drops.add(seeds);
        }

        ItemStack originalSeeds = new ItemStack(seeds);
        originalSeeds.setAmount(1);
        drops.add(originalSeeds);

        return drops;
    }

    @Override
    public void applyGrowthTick(Block block, CropStats stats, GrowthProgress growthProgress) {
        if (growthProgress.getGrowthFraction() <= getStemMaxGrowthFraction())
            if (block.getType() != growing_stem)
                block.setType(growing_stem);

        if (block.getType() == growing_stem) {
            Ageable ageable = (Ageable) block.getBlockData();
            double stemGrowthProgress = growthProgress.getGrowthFraction() / getStemMaxGrowthFraction();
            int targetAge = (int) Math.ceil(ageable.getMaximumAge() * Math.min(stemGrowthProgress, 1));
            if (ageable.getAge() != targetAge) {
                ageable.setAge(targetAge);
                block.setBlockData(ageable);
            }
        }

        if (growthProgress.getGrowthFraction() < 1) return;
        if (block.getType() == attached_stem && ((Directional) block.getBlockData()).getFacing() != BlockFace.SELF)
            return;

        List<BlockFace> faces = new ArrayList<>(Arrays.asList(
            BlockFace.NORTH,
            BlockFace.SOUTH,
            BlockFace.EAST,
            BlockFace.WEST
        ));

        BlockFace face = null;
        while (faces.size() > 0) {
            BlockFace testFace = faces.remove((int) (faces.size() * Math.random()));
            Material mat = block.getRelative(testFace).getType();
            boolean solidBase = block
                .getRelative(testFace)
                .getRelative(0, -1, 0)
                .getType()
                .isSolid();
            if (mat.isAir() && solidBase) {
                face = testFace;
                break;
            }
        }
        if (face == null)
            return;

        block.setType(attached_stem);

        Block gourdLocation = block.getRelative(face);
        growthProgress.createChildCrop(gourdLocation, "gourd");

        Directional dir = (Directional) block.getBlockData();
        dir.setFacing(face);
        block.setBlockData(dir);

        growthProgress.setGrowthFraction(getStemMaxGrowthFraction());
    }

    @Override
    public boolean canGrow(Block block) {
        return block.getType() == growing_stem;
    }

    /**
     * @return the fraction of growth time spent on the stem (0-1)
     */
    protected abstract double getStemMaxGrowthFraction();

    @Override
    public boolean plant(Block block) {
        Material below = block.getRelative(0, -1, 0).getType();
        if (below != Material.FARMLAND)
            return false;
        block.setType(growing_stem);
        return true;
    }

    @Override
    public boolean isValid(Block block) {
        return block.getType() == growing_stem || block.getType() == attached_stem;
    }

    @Override
    public boolean isValidSeeds(ItemStack item) {
        return item.getType() == seeds;
    }

    @Override
    public double waterUsage() {
        return BalanceConstants.BASE_WATER_USAGE;
    }
}
