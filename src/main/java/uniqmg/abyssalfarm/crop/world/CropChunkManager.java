package uniqmg.abyssalfarm.crop.world;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.world.*;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.abyssalfarm.AbyssalFarm;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class CropChunkManager implements Listener {
    private final Map<Chunk, CropChunk> cropMap = new HashMap<>();
    private final AbyssalFarm plugin;
    private BukkitRunnable updateTask;

    public CropChunkManager(AbyssalFarm plugin) {
        this.plugin = plugin;
        this.startUpdateTask();

        // Enable any chunks loaded before the plugin was enabled
        for (World world: plugin.getServer().getWorlds())
            for (Chunk chunk: world.getLoadedChunks())
                this.getCropChunk(chunk);
    }

    private void startUpdateTask() {
        if (this.updateTask != null)
            throw new IllegalStateException("Update task already started");

        this.updateTask = new BukkitRunnable() {
            @Override
            public void run() {
                cropMap.forEach((chunk, cropChunk) -> cropChunk.growthTick());
            }
        };
        this.updateTask.runTaskTimer(plugin, 0, plugin.getCropTickRate());
    }

    /**
     * Returns the config file for a given chunk's CropChunk
     * @param chunk the chunk whose config file to retrieve
     * @return the retrieved config file
     */
    private File chunkFile(Chunk chunk) {
        return Paths.get(
            plugin.getDataFolder().toPath().toString(),
            "chunks",
            chunk.getWorld().getName(),
            String.format(
                "chunk_%d_%d.yml",
                chunk.getX(),
                chunk.getZ()
            )
        ).toFile();
    }

    /**
     * Gets a CropChunk for a given chunk. The chunk is either returned from memory,
     * loaded from disk, or created and returned.
     *
     * @param chunk the chunk whose CropChunk to fetch
     * @return the CropChunk for the given chunk
     */
    public CropChunk getCropChunk(Chunk chunk) {
        // Return loaded
        if (cropMap.containsKey(chunk))
            return cropMap.get(chunk);

        // Load from file
        File chunkFile = chunkFile(chunk);
        if (chunkFile.exists()) {
//            System.out.println("Loaded crop chunk at " + chunk);
            YamlConfiguration config = YamlConfiguration.loadConfiguration(chunkFile);
            CropChunk cropChunk = (CropChunk) config.get("chunk");
            assert cropChunk != null;
            cropMap.put(chunk, cropChunk);
            return cropChunk;
        }

        // Create new
//        System.out.println("Created new crop chunk at " + chunk);
        CropChunk cropChunk = new CropChunk();
        cropMap.put(chunk, cropChunk);
        return cropChunk;
    }

    /**
     * Gets a crop at a given location
     * @param location the location
     * @return the crop at the location, or null
     */
    public CropData getCrop(Location location) {
        CropChunk cropChunk = this.getCropChunk(location.getChunk());
        CropData cropData = cropChunk.cropAt(location);
        if (cropData == null || !cropData.isValid(location.getBlock()))
            return null;
        return cropData;
    }

    /**
     * Saves a CropChunk to disk
     *
     * @param chunk the chunk identifying the CropChunk
     * @param unload if true, unloads the CropChunk from memory
     * @throws IOException if saving the cropChunk fails
     */
    public void flushChunk(Chunk chunk, boolean unload) throws IOException {
        File chunkFile = chunkFile(chunk);
        YamlConfiguration chunkData = new YamlConfiguration();

        if (!chunkFile.exists())
            Files.createDirectories(chunkFile.toPath().getParent());

        CropChunk cropChunk = getCropChunk(chunk);
        if (cropChunk != null)
            cropChunk.cleanupGarbage();

        if (cropChunk == null || cropChunk.isEmpty()) {
            if (chunkFile.exists() && !chunkFile.delete())
                throw new IOException("Failed to delete " + chunkFile);

            if (unload)
                cropMap.remove(chunk);
            return;
        }

        chunkData.set("chunk", cropChunk);
        chunkData.save(chunkFile);
        if (unload)
            cropMap.remove(chunk);
    }

    /**
     * Saves all current CropChunks
     * @throws IOException if saving failed
     */
    public void saveAllChunks() throws IOException {
        for (Map.Entry<Chunk, CropChunk> entry : cropMap.entrySet())
            this.flushChunk(entry.getKey(), false);
    }

    @EventHandler
    public void onWorldSave(WorldSaveEvent evt) throws IOException {
        for (Chunk chunk: evt.getWorld().getLoadedChunks())
            this.flushChunk(chunk, false);
    }

    @EventHandler
    public void onChunkLoad(ChunkLoadEvent evt) {
        this.getCropChunk(evt.getChunk());
    }

    @EventHandler
    public void onChunkUnload(ChunkUnloadEvent evt) throws IOException {
        if (!evt.isSaveChunk()) return;
        this.flushChunk(evt.getChunk(), true);
    }

    @EventHandler
    public void onGrowthTick(BlockGrowEvent evt) {
        Block block = evt.getBlock();
        block.getChunk();
        evt.setCancelled(true);
    }
}