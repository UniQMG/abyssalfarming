package uniqmg.abyssalfarm.crop.world.hydration;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.Levelled;
import org.bukkit.util.Vector;
import uniqmg.abyssalfarm.AbyssalFarm;

import java.util.*;

import static uniqmg.abyssalfarm.crop.world.hydration.WaterSearchResult.WaterConsumeState.*;

/**
 * An iterator for locating water connected to a crop
 */
public class WaterFinder implements Iterator<WaterSearchResult> {
    /**
     * The search queue stores locations that need to be inspected next. When a block is inspected,
     * if it's a water block, it's neighbors are added to the search queue.
     */
    private ArrayDeque<Location> searchQueue = new ArrayDeque<>();
    /**
     * The result queue stores results that should be returned next. Since results are discovered at
     * up to eight at a time, the result queue is filled up then emptied before finding more results.
     */
    private ArrayDeque<WaterSearchResult> resultQueue = new ArrayDeque<>();
    /**
     * The searched set stores previously searched locations preventing infinite loops.
     */
    private Set<Location> searched = new HashSet<>();
    /**
     * The next result is a result cached for tryNext() lookups and may be null if next() is called.
     */
    private WaterSearchResult next = null;
    /**
     * Iteration count to prevent excessively long searches in a large body of flowing water. Stores the
     * number of Locations inspected, not neighbors of those locations.
     */
    private int iterations;

    /**
     * Creates a new water finder. Note that unless an agent is specified, the initial search block
     * is the block directly below the given crop block (i.e. the dirt/farmland the crop is on).
     *
     * @param cropBlock where to start looking. Discovery starts on a 5x5 grid centered on the block BELOW this one.
     * @param agent An agent providing a relative initial search location.
     */
    public WaterFinder(Block cropBlock, WaterSearchLocationModifier agent) {
        Location location = (agent != null)
            ? agent.getInitialLocation(cropBlock.getLocation())
            : cropBlock.getLocation().add(0, -1, 0);

        for (Vector vec: initialSearch)
            this.searchQueue.add(location.clone().add(vec));
    }

    private final static List<Vector> initialSearch = new ArrayList<>();
    static {
        for (int x = -2; x <= 2; x++) {
            for (int z = -2; z <= 2; z++) {
                if (x == 0 && z == 0) continue;
                initialSearch.add(new Vector(x, 0, z));
            }
        }
    }

    private Location[] getNeighbors(Location loc) {
        return new Location[] {
            loc.clone().add( 1,  0,  0),
            loc.clone().add(-1,  0,  0),
            loc.clone().add( 0,  1,  0),
            loc.clone().add( 0, -1,  0),
            loc.clone().add( 0,  0,  1),
            loc.clone().add( 0,  0, -1),
        };
    }

    private WaterSearchResult findNext() {
        while (resultQueue.size() == 0 && searchQueue.size() > 0) {
            if (iterations++ > AbyssalFarm.plugin.getWaterSearchDepth()) {
                return new WaterSearchResult(FAILED, null);
            }

            Location loc = searchQueue.removeFirst();
            for (Location neighbor: this.getNeighbors(loc)) {
                if (neighbor.getBlockY() < 0 || neighbor.getBlockY() >= 256)
                    continue;

                if (searched.contains(neighbor)) continue;
                searched.add(neighbor);

                // If a neighbor isn't loaded, it might mean the water supply is partially
                // in a neighboring unloaded chunk.
                int chunk_x = neighbor.getBlockX() >> 4;
                int chunk_z = neighbor.getBlockZ() >> 4;
                if (!neighbor.getWorld().isChunkLoaded(chunk_x, chunk_z)) {
                    searchQueue.addFirst(loc);
                    return new WaterSearchResult(UNLOADED, null);
                }

                Block block = neighbor.getBlock();
                if (block.getType() == Material.WATER) {
                    Levelled level = (Levelled) block.getBlockData();
                    searchQueue.add(neighbor);

                    Block above = block.getRelative(0, 1, 0);

                    if (above.getType() == Material.WATER && ((Levelled) above.getBlockData()).getLevel() == 0) {
                        // Source block has another source block above it
                        // Don't eat this one until the one above it is gone
                        continue;
                    }

                    if (level.getLevel() == 0) {
                        // Valid consumable source block
                        resultQueue.add(new WaterSearchResult(OK, block));
                    }

                    // Non-source water block, keep searching
                }
            }
        }

        if (resultQueue.size() > 0) {
            return resultQueue.removeFirst();
        }

        return new WaterSearchResult(FAILED, null);
    }

    /**
     * Checks if there's an available water source block for consumption. next() will always
     * return the next search result, hasNext() only determines if it's a valid one. An
     * unloaded chunk is considered invalid.
     *
     * @return whether there's an available water source block to consume
     */
    @Override
    public boolean hasNext() {
        if (next == null)
            next = findNext();

        return !next.failed();
    }

    /**
     * Finds and consumes a water source block. Searching is started on the block below
     * the original block, and water blocks are followed in all 6 directions until a
     * source block is found. Search position and history is retained between calls.
     *
     * @return the result of the next search.
     */
    @Override
    public WaterSearchResult next() {
        if (next == null)
            next = findNext();

        if (next.isConsumed())
            next = findNext();

        try {
            return next;
        } finally {
            next = null;
        }
    }
}
