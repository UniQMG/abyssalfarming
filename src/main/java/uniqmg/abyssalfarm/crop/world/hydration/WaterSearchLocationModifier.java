package uniqmg.abyssalfarm.crop.world.hydration;

import org.bukkit.Location;

public interface WaterSearchLocationModifier {
    /**
     * Modifies the initial starting location for the WaterFinder
     * @param start the initial starting location
     * @return the desired initial starting location
     */
    Location getInitialLocation(Location start);
}
