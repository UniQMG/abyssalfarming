package uniqmg.abyssalfarm.crop.world.hydration;

import org.bukkit.block.Block;
import org.bukkit.block.data.Levelled;

import static uniqmg.abyssalfarm.crop.world.hydration.WaterSearchResult.WaterConsumeState.*;

public class WaterSearchResult {
    private final WaterConsumeState state;
    private final Block block;
    private boolean consumed;

    enum WaterConsumeState {
        /**
         * If water was found and is consumable
         */
        OK,
        /**
         * If water wasn't found
         */
        FAILED,
        /**
         * If an unloaded chunk was encountered while
         * searching for water
         */
        UNLOADED
    }

    public WaterSearchResult(WaterConsumeState state, Block block) {
        this.state = state;
        this.block = block;
        this.consumed = false;
    }

    public boolean failed() {
        return this.state == FAILED || hitUnloadedChunk();
    }

    public boolean hitUnloadedChunk() {
        return this.state == UNLOADED;
    }

    public boolean foundSourceBlock() {
        return this.state == OK;
    }

    public boolean isConsumed() {
        return consumed;
    }

    public void consume() {
        if (consumed)
            throw new IllegalStateException("Already consumed water from the water search");
        if (!foundSourceBlock())
            throw new IllegalStateException("Cannot consume water from a failed water search");

        Levelled level = (Levelled) block.getBlockData();
        level.setLevel(1);
        block.setBlockData(level);
        consumed = true;
    }

    @Override
    public String toString() {
        if (this.foundSourceBlock())
            return String.format("%s (block: %s, consumed: %s)", this.state, this.block, this.consumed);

        return String.format("%s", this.state);
    }
}
