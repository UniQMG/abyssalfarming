package uniqmg.abyssalfarm.crop.world;

import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import uniqmg.abyssalfarm.AbyssalFarm;

public class CropUtil {
    public static boolean isOcean(Biome biome) {
        switch (biome) {
            case OCEAN:
            case COLD_OCEAN:
            case DEEP_COLD_OCEAN:
            case DEEP_FROZEN_OCEAN:
            case DEEP_LUKEWARM_OCEAN:
            case DEEP_OCEAN:
            case DEEP_WARM_OCEAN:
            case FROZEN_OCEAN:
            case LUKEWARM_OCEAN:
            case WARM_OCEAN:
                return true;

            default:
                return false;
        }
    }

    public static double getComputedTemperature(Block block) {
        return block.getTemperature() + (isGreenhouse(block) ? AbyssalFarm.plugin.getGreenhouseHeatBonus() : 0);
    }

    public static boolean isGreenhouse(Block block) {
        byte light = block.getLightFromSky();
        if (light < 15) return false;

        int steps = 0;
        while (block.getType() != Material.GLASS) {
            block = block.getRelative(0, 1, 0);
            if (++steps >= 6) return false;
        }

        return true;
    }
}
