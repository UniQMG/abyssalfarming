package uniqmg.abyssalfarm.crop.world;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import uniqmg.abyssalfarm.AbyssalFarm;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;

import java.util.*;

public class CropChunk implements ConfigurationSerializable {
    /**
     * Master list of crops in the chunk
     */
    private Map<Location, CropData> crops = new HashMap<>();

    /**
     * Filtered version of crop keys that only includes active crops
     */
    private Set<Location> activeCrops = new HashSet<>();

    /**
     * A sorted queue of crops and locations sorted by next visual update tick.
     */
    private PriorityQueue<CropVisualTickTracker> visualTickQueue = new PriorityQueue<>();

    /**
     * Sets up CropData for a block and returns it
     * @param block The block to set up CropData for
     * @param seeds The seeds to create a crop from
     * @return the new CropData, or null if it wasn't a crop
     */
    public CropData initializeCrop(Block block, ItemStack seeds) {
        CropType type = AbyssalFarm.plugin.getCropRegistry().fromSeeds(seeds);
        if (type == null) return null;
        CropStats stats = CropStats.fromStack(seeds);
        return initializeCrop(block, type, stats);
    }

    /**
     * Sets up CropData for a block and returns it
     * @param block the block to set up CropData for
     * @param type the type of crop to initialize the block to
     * @param stats the stats for the new crop
     * @return the new CropData, or null if it wasn't a corp
     */
    public CropData initializeCrop(Block block, CropType type, CropStats stats) {
        CropData cropData = new CropData(type, stats);
        if (!cropData.crop.plant(block))
            return null;
        this.initializeCrop(block.getLocation(), cropData);
        return cropData;
    }

    /**
     * Registers the given crop to the crop map
     * @param loc The location of the crop. Must be floored.
     * @param cropData the crop's data
     */
    private void initializeCrop(Location loc, CropData cropData) {
        crops.put(loc, cropData);
        if (!cropData.crop.isPassive())
            activeCrops.add(loc);
        visualTickQueue.add(new CropVisualTickTracker(loc, cropData));
    }

    /**
     * Removes a crop from the crop map and breaks and drops the crop's drops.
     * If the passed block is not a crop, it is broken naturally instead.
     *
     * @param block the crop containing block
     * @return the removed crop, or null
     */
    public CropData breakCrop(Block block) {
        CropData crop = crops.remove(block.getLocation());
        if (crop == null) {
            block.breakNaturally();
            return null;
        }

        block.setType(Material.AIR);
        for (ItemStack drop: crop.getDrops())
            block.getWorld().dropItemNaturally(block.getLocation(), drop);

        return crop;
    }

    /**
     * Passes time for all the non-passive crops in this chunk.
     * See {@link CropData#growthTick(Block)}
     */
    public void growthTick() {
        Iterator<Location> iterator = this.activeCrops.iterator();
        while (iterator.hasNext()) {
            Location next = iterator.next();
            CropData crop = crops.get(next);
            Block block = next.getBlock();

            if (crop == null || !crop.isValid(block) || crop.crop.isPassive()) {
                iterator.remove();
                continue;
            }

            crop.growthTick(block);
        }
    }

    /**
     * Performs visual ticks for all crops in this chunk
     */
    public void visualTick() {
        int ticked = 0;
        while (visualTickQueue.size() > 0) {
            if (ticked > 100) break;

            CropVisualTickTracker tracker = visualTickQueue.peek();
            ticked++;

            if (!tracker.isReadyToTick())
                break;

            // Remove the element now that we're going to do something with it
            visualTickQueue.poll();

            CropData cropData = crops.get(tracker.getLocation());
            if (cropData == null || !cropData.isValid(tracker.getLocation().getBlock()))
                tracker.markExpired();

            if (tracker.isExpired())
                continue;

            tracker.visualTick();
            visualTickQueue.add(tracker);
        }
    }

    /**
     * Gets a crop at a given location.
     * @param location the location to get the crop for
     * @return the CropData, or null
     */
    public CropData cropAt(Location location) {
        return crops.get(location.getBlock().getLocation());
    }

    /**
     * Removes invalid crops from the crop set
     */
    public void cleanupGarbage() {
        crops.entrySet().removeIf(entry -> {
            Location location = entry.getKey();
            CropData cropData = entry.getValue();
            if (!cropData.isValid(location.getBlock()))
                return true;
            return false;
        });
    }

    public boolean isEmpty() {
        return crops.size() == 0;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
        data.put("crop", crops);
        return data;
    }

    @SuppressWarnings("unused")
    public static CropChunk deserialize(Map<String, Object> data) {
        CropChunk cropChunk = new CropChunk();
        //noinspection unchecked
        Map<Location, CropData> chunkData = (Map<Location, CropData>) data.get("crop");
        chunkData.forEach(cropChunk::initializeCrop);
        return cropChunk;
    }
}
