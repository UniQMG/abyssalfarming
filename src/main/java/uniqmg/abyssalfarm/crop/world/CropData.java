package uniqmg.abyssalfarm.crop.world;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import uniqmg.abyssalfarm.AbyssalFarm;
import uniqmg.abyssalfarm.crop.CropStats;
import uniqmg.abyssalfarm.crop.CropType;
import uniqmg.abyssalfarm.crop.GrowthProgress;
import uniqmg.abyssalfarm.crop.crops.custom.OverrideCrop;
import uniqmg.abyssalfarm.crop.world.hydration.WaterFinder;
import uniqmg.abyssalfarm.crop.world.hydration.WaterSearchLocationModifier;
import uniqmg.abyssalfarm.crop.world.hydration.WaterSearchResult;

import java.util.*;
import java.util.stream.Collectors;

public class CropData implements ConfigurationSerializable, GrowthProgress {
    /**
     * This crop's underlying crop type
     */
    public final CropType crop;
    /**
     * When the CropData was last ticked
     */
    private long lastTick = -1;
    /**
     * The current health of the crop
     */
    private double health;
    /**
     * The growth progress of the crop, in ticks
     */
    private int growth = 0;
    /**
     * The current hydration level of the crop, in mB (1000mB/bucket, 1 bucket/block)
     */
    private double hydration = 0;
    /**
     * The crop's stats
     */
    private CropStats stats = new CropStats();
    /**
     * The amount of hydration, in mB, provided per source block consumed
     */
    private static final int HYDRATION_PER_SOURCE_BLOCK = 1000;
    /**
     * The minimum hydration that is auto-set when it's raining on the crop
     */
    private static final int RAIN_HYDRATION = 1000;

    public CropData(CropType crop, CropStats stats) {
        this.crop = crop;
        if (stats != null)
            this.stats = stats;

        this.health = this.maxHealth();
    }

    /**
     * Gets the items dropped by this CropData on being destroyed
     * @param growth The growth progress to get drops at
     * @return a list of dropped items
     */
    public List<ItemStack> getDropsAtGrowth(double growth) {
        double growthProgress = Math.min(growth / (double) crop.getMaxGrowthProgress(), 1);

        return this.crop
            .getDrops(this.stats, growthProgress)
            .stream()
            .filter(itemStack -> itemStack.getAmount() != 0)
            .collect(Collectors.toList());
    }

    /**
     * Gets the items dropped by this CropData on being destroyed at the current growth stage
     * @return a list of dropped items
     */
    public List<ItemStack> getDrops() {
        return this.getDropsAtGrowth(this.growth);
    }

    /**
     * Checks if the crop is valid for the given block
     * @param block the block to check validity for
     * @return if the crop is valid
     */
    public boolean isValid(Block block) {
        return this.crop.isValid(block);
    }

    /**
     * Passes time, causing the crop to grow and execute other related behaviors.
     * The number of ticks is automatically determined from the world time.
     * @param block The block associated with this CropData
     */
    public void growthTick(Block block) {
        // Crops can be broken in an undetected way, so checking if they're
        // still the expected block is good practice. Invalid crop are
        // garbage-collected on save.
        if (!this.isValid(block))
            return;

        if (this.health <= 0) {
            this.health = 0;
            block.setType(Material.DEAD_BUSH);
            return;
        }

        long currentTime = block.getWorld().getFullTime();
        if (this.lastTick == -1 || this.lastTick > currentTime) // Not set or in the future
            this.lastTick = currentTime;
        final long dt = currentTime - lastTick;

        /* Pre-grow stage */
        int maxGrowthProgress = this.crop.getMaxGrowthProgress();

        double effectiveWaterUsage = (
            AbyssalFarm.plugin.cropsGrownRequireWater() ||
            this.growth < maxGrowthProgress
        ) ? crop.waterUsage() : 0;

        if (CropUtil.isGreenhouse(block))
            effectiveWaterUsage *= AbyssalFarm.plugin.getGreenhouseWaterMultiplier();

        long maxUsableHydration = (long) Math.ceil(dt * effectiveWaterUsage * stats.waterUsageMultiplier());

        CropType baseCrop = crop instanceof OverrideCrop
            ? ((OverrideCrop) crop).getNestedBase()
            : crop;
        WaterSearchLocationModifier agent = baseCrop instanceof WaterSearchLocationModifier
            ? (WaterSearchLocationModifier) baseCrop
            : null;

        // Hydration from rain
        if (block.getWorld().hasStorm() && block.getWorld().getHighestBlockYAt(block.getLocation()) == block.getY()) {
            this.hydration = Math.max(this.hydration, RAIN_HYDRATION);
        }

        boolean consumedWater = false;
        WaterFinder finder = new WaterFinder(block, agent);
        while (this.hydration < maxUsableHydration) {
            WaterSearchResult result = finder.next();

            // Insufficient water
            if (result.failed()) break;

            // Stall crop growth if the water supply is partially unloaded
            if (result.hitUnloadedChunk()) return;

            if (result.foundSourceBlock()) {
                if (!consumedWater) {
                    consumedWater = true;
                    block.getWorld().spawnParticle(
                        Particle.WATER_DROP,
                        block.getLocation(),
                        5,
                        0,
                        0,
                        0
                    );
                }

                result.consume();
                this.hydration += HYDRATION_PER_SOURCE_BLOCK;
            }
        }

        byte light = block.getType().isSolid()
            ? block.getRelative(0, 1, 0).getLightFromSky()
            : block.getLightFromSky();
        double lightMultiplier = light >= 5
            ? 1 - (10 - (light - 5)) / 10d
            : 0;

        /* Grow stage */
        if (effectiveWaterUsage > 0) {
            long maxGrowableTicks = (long) (this.hydration / (crop.waterUsage() * stats.waterUsageMultiplier()));
            long growableTicks = Math.max(Math.min(dt, maxGrowableTicks), 0); // Ticks spent growing
            long parchedTicks = dt - growableTicks; // Ticks spent without sufficient water to grow or in improper climate

            if (!supportsClimate(block)) {
                parchedTicks += growableTicks;
                growableTicks = 0;
            }

            growableTicks = (long) Math.ceil(growableTicks * lightMultiplier);
            if (!crop.canGrow(block))
                growableTicks = 0;

            this.growth += growableTicks * stats.growthMultiplier();
            this.hydration -= growableTicks * crop.waterUsage() * stats.waterUsageMultiplier();
            this.setHealth(this.health - parchedTicks / 200d);
        } else {
            if (!supportsClimate(block))
                this.setHealth(this.health - dt / 200d);

            else if (crop.canGrow(block))
                this.growth += dt * stats.growthMultiplier() * lightMultiplier;
        }

        /* Post-grow stage */
        if (growth > maxGrowthProgress)
            growth = maxGrowthProgress;

        Map<String, CropType> canonicalChildren = this.crop
            .getChildren()
            .entrySet()
            .stream()
            .collect(Collectors.toMap(
                Map.Entry::getKey,
                entry -> {
                    String name = entry.getValue().getName();
                    return AbyssalFarm.plugin.getCropRegistry().fromName(name);
                }
            ));

        this.crop.applyGrowthTick(block, stats, this);
        this.lastTick += dt;
    }

    @Override
    public double getGrowthFraction() {
        double progress = (double) this.growth / crop.getMaxGrowthProgress();
        return Math.min(Math.max(progress, 0), 1);
    }

    @Override
    public void setGrowthFraction(double progress) {
        progress = Math.min(Math.max(progress, 0), 1);
        this.growth = (int) (progress * crop.getMaxGrowthProgress());
    }

    @Override
    public void createChildCrop(Block block, String name) {
        CropType childCrop = Objects.requireNonNull(this.crop.getChildren().get(name));
        CropType canonical = AbyssalFarm.plugin.getCropRegistry().fromName(childCrop.getName());

        // TODO: Turn this into an internal queue processed after applying growth ticks
        new BukkitRunnable() {
            @Override
            public void run() {
                AbyssalFarm.plugin
                    .getCropChunkManager()
                    .getCropChunk(block.getChunk())
                    .initializeCrop(block, canonical, stats);
            }
        }.runTask(AbyssalFarm.plugin);
    }

    /**
     * Checks if the crop supports the climate at the given block. Crops have a preferred temperature and a tolerance
     * range based on stats and nature. Additionally, some biomes (i.e. oceans) don't support any crops at all.
     * @param block the block to check climate at
     * @return whether the climate is supported
     */
    public boolean supportsClimate(Block block) {
        if (CropUtil.isOcean(block.getBiome()))
            return false;

        double target = crop.getPreferredTemperature();
        double range = crop.getTemperatureTolerance() + stats.adaptabilityRange();
        double temperature = CropUtil.getComputedTemperature(block);
        double difference = Math.abs(temperature - target);
        return difference < range;
    }

    public int getGrowth() {
        return growth;
    }

    public void setGrowth(int growth) {
        if (growth < 0) growth = 0;
        this.growth = growth;
    }

    public double getHealth() {
        return this.health;
    }

    public void setHealth(double health) {
        this.health = Math.min(Math.max(health, 0), maxHealth());
    }

    public double maxHealth() {
        return this.stats.healthMultiplier() * 1000d;
    }

    public double getHydration() {
        return hydration;
    }

    public CropStats getStats() {
        return stats;
    }

    public void setStats(CropStats stats) {
        this.stats = stats;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();
        data.put("crop", crop.getName());
        data.put("stats", stats);

        data.put("lastTick", lastTick);
        data.put("health", health);
        data.put("growth", growth);
        data.put("hydration", hydration);
        return data;
    }

    @SuppressWarnings("unused")
    public static CropData deserialize(Map<String, Object> map) {
        CropType cropType = AbyssalFarm.plugin.getCropRegistry().fromName((String) map.get("crop"));
        CropStats stats = (CropStats) map.getOrDefault("stats", new CropStats());

        CropData cropData = new CropData(cropType, stats);

        cropData.lastTick = Long.valueOf(map.getOrDefault("lastTick", -1L).toString());
        cropData.health = Double.valueOf(map.getOrDefault("health", 1000d).toString());
        cropData.growth = Integer.valueOf(map.getOrDefault("growth", 0).toString());
        cropData.hydration = Double.valueOf(map.getOrDefault("hydration", 0d).toString());

        return cropData;
    }

    @Override
    public String toString() {
        return String.format("%s (Growth: %d / %d)", crop, growth, crop.getMaxGrowthProgress());
    }
}
