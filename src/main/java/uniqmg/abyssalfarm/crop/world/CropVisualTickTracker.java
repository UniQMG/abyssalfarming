package uniqmg.abyssalfarm.crop.world;

import org.bukkit.Location;
import org.bukkit.block.Block;

/**
 * Tracks crop visual ticks for a CropChunk. Should be completely invisible to
 * the CropData it's tracking.
 */
public class CropVisualTickTracker implements Comparable<CropVisualTickTracker> {
    /**
     * The location of the crop
     */
    private final Location location;

    /**
     * The CropData this tick manager is tracking ticks for
     */
    private final CropData cropData;

    /**
     * When the crop is next scheduled to receive a visual tick
     */
    private long nextVisualTick = -1;

    public CropVisualTickTracker(Location location, CropData cropData) {
        this.location = location.clone();
        this.cropData = cropData;
    }

    /**
     * If the tick tracker has expired. The tick tracker expires when the
     * next visual tick is disabled (negative value) or when the crop is
     * invalidated.
     */
    private boolean expired = false;
    /**
     * Marks the tracker as expired
     */
    public void markExpired() {
        this.expired = true;
    }
    /**
     * @return if the tracker has been marked as expired
     */
    public boolean isExpired() {
        return expired;
    }

    /**
     * Checks if the crop is ready to receive a visual tick. Doesn't consider expiration.
     * @return if the crop is ready to be visual ticked
     */
    public boolean isReadyToTick() {
        return location.getBlock().getWorld().getFullTime() >= this.nextVisualTick;
    }

    /**
     * Runs the underlying crop's visual tick
     */
    public void visualTick() {
        Block block = location.getBlock();

        long time = block.getWorld().getFullTime();

        int nextTick_dt = cropData.crop.visualTick(block);
        if (nextTick_dt < 0) {
            this.markExpired();
            this.nextVisualTick = 0;
            return;
        }
        this.nextVisualTick = time + nextTick_dt;
    }

    @Override
    public int compareTo(CropVisualTickTracker other) {
        if (other.nextVisualTick == this.nextVisualTick) return 0;
        return other.nextVisualTick > this.nextVisualTick ? -1 : 1;
    }

    public Location getLocation() {
        return location;
    }
}